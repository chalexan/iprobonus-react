import './App.css';
import 'antd/dist/antd.css';
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { Redirect } from "react-router-dom";
import SignInPage from './pages/SignInPage/SignInPage';
import SignUpPage from './pages/SignUpPage/SignUpPage';
import LayoutPage from './components/LayoutPage/LayoutPage';
import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
require('dotenv').config();

function App() {
  require('dotenv').config();
  const { token } = useSelector((state) => state);
  const dispatch = useDispatch();
  useEffect(() => {
    tryAuth();
  }, []);

  const tryAuth = async () => {
    if (localStorage.getItem('token')) {
      dispatch({
        type: "LOGIN",
        payload: { token: localStorage.getItem('token'), username: localStorage.getItem('username') },
      });
    }

  }

  return (
    <div>
      <BrowserRouter>
        <Switch className='container'>
          <Route path='/login'>
            <SignInPage />
          </Route>
          <Route path='/registration'>
            <SignUpPage />
          </Route>
          <Route path='/MareketingEvent/New'>
            <LayoutPage page="MarketingEvent" />
          </Route>
          <Route path='/Clients/List'>
            <LayoutPage page="ClientsList" />
          </Route>
          <Route path='/Clients/Info'>
            <LayoutPage page="ClientInfo" />
          </Route>
          <Route path='/Clients/Search'>
            <LayoutPage page="ClientSearch" />
          </Route>
          <Route path='/Enterprises/List'>
            <LayoutPage page="EnterprisesList" />
          </Route>
          <Route path='/Enterprises/Info'>
            <LayoutPage page="EnterprisesInfo" />
          </Route>
          <Route path='/Enterprises/New'>
            <LayoutPage page="EnterprisesNew" />
          </Route>
          <Route path='/Users/New'>
            <LayoutPage page="UsersCreate" />
          </Route>
          <Route path='/Shops/New'>
            <LayoutPage page="ShopsNew" />
          </Route>
          <Route path='/Shops/List'>
            <LayoutPage page="ShopsList" />
          </Route>
          <Route path='/Shops/City/New'>
            <LayoutPage page="CityNew" />
          </Route>
          <Route path='/Shops/Info'>
            <LayoutPage page="ShopsInfo" />
          </Route>
          <Route path='/Product/Info/'>
            <LayoutPage page="ProductInfo" />
          </Route>
          <Route path='/PaymentList'>
            <LayoutPage page="PaymentList" />
          </Route>
        </Switch>
        {!token ? <Redirect to={"/login"} /> : <Redirect to={'/Enterprises/List'} />}
      </BrowserRouter>
    </div>
  );
}

export default App;
