import "./PaymentList.css";
import * as constant from "../constant";
import { Card, Button, Table, message } from "antd";
import { Breadcrumb, Radio } from "antd";
import { HomeOutlined, SearchOutlined, UserOutlined } from "@ant-design/icons";
import { Link, useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { FaUserTie } from "react-icons/fa";
import { useEffect, useState } from "react";
import moment from "moment";
import Highlighter from "react-highlight-words";

const PaymentList = () => {
  const winWidth = window.innerWidth;
  const dispatch = useDispatch();
  const history = useHistory();
  const [spinner, setSpinner] = useState(false);
  const [docs, setDocs] = useState(null);
  const [source, setSourse] = useState([]);
  const [confirmed, setConfirmed] = useState(false);

  useEffect(() => {
    getPaymentList();
  }, []);

  // получение списка документов с API

  const getPaymentList = async () => {
    const token = localStorage.getItem("token");
    const api = constant.APIPAYMENT;
    setSpinner(true);
    const response = await fetch(`${api}/moneyout/list`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `bearer ${token}`,
        AccessKey: constant.ACCESSKEY,
      },
    });
    setSpinner(false);
    if (response.status === 401) {
      return dispatch({ type: "LOGOUT" });
    }
    if (!response.ok) {
      dispatch({
        type: "SET_DEBUG_MESSAGE",
        payload: response.statusText,
      });
      return message.error("Произошла ошибка");
    }
    const result = await response.json();

    // формат даты
    const modifyTime = result.dataList.map((item) => {
      item.dateDoc = moment(item.dateDoc).format("LLL");
      item.dateСonfirmation = moment(item.dateСonfirmation).format("LLL");
      return item;
    });
    const modifyTime2 = modifyTime.map((item) => {
      item.dateСonfirmation = moment(item.dateСonfirmation).format("LLL");
      return item;
    });
    console.log(modifyTime2);
    setSourse(modifyTime2);
  };

  // Модификация списка

  const modifyTable = (value) => {
    setConfirmed(value);
    if (value === "true") {
      // изменить источник таблицы отфильтровав только DateСonfirmation !== null
      getPaymentList();
      const newList = source.filter(
        (item) => item.dateСonfirmation !== "Invalid date"
      );
      console.log("notComfirmed", newList);
      setDocs(newList);
    } else {
      // изменить источник таблицы отфильтровав только DateСonfirmation === null
      getPaymentList();
      const newList = source.filter(
        (item) => item.dateСonfirmation == "Invalid date"
      );
      console.log("Comfirmed", newList);
      setDocs(newList);
    }
  };

  // Подтверждение оплаты

  const acceptHandler = async (record) => {
    const token = localStorage.getItem("token");
    const api = constant.APIPAYMENT;
    setSpinner(true);

    const response = await fetch(
      `${api}/moneyout/confirm/${record.idUnique}/${record.idOfficerConfirmation}`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `bearer ${token}`,
          AccessKey: constant.ACCESSKEY,
        },
      }
    );
    setSpinner(false);
    if (response.status === 401) {
      return dispatch({ type: "LOGOUT" });
    }
    if (!response.ok) {
      dispatch({
        type: "SET_DEBUG_MESSAGE",
        payload: responce.statusText,
      });
      return message.error("Произошла ошибка");
    }

    message.info("Подтверждено");

    const responce = await fetch(`${api}/moneyout/list`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `bearer ${token}`,
        AccessKey: constant.ACCESSKEY,
      },
    });
    setSpinner(false);
    if (responce.status === 401) {
      return dispatch({ type: "LOGOUT" });
    }
    if (!responce.ok) {
      dispatch({
        type: "SET_DEBUG_MESSAGE",
        payload: responce.statusText,
      });
      return message.error("Произошла ошибка");
    }
    const result = await responce.json();

    // формат даты
    const modifyTime = result.dataList.map((item) => {
      item.dateDoc = moment(item.dateDoc).format("LLL");
      item.dateConfirmation = moment(item.dateConfirmation).format("LLL");
      return item;
    });
    const modifyTime2 = modifyTime.map((item) => {
      item.dateСonfirmation = moment(item.dateСonfirmation).format("LLL");
      return item;
    });

    setSourse(modifyTime2);
    const newList = modifyTime2.filter(
      (item) => item.dateСonfirmation !== null
    );
    setDocs(newList);
  };

  const columns = [
    {
      title: "Дата",
      render: (_, record) => {
        return <b>{record.dateDoc}</b>;
      },
    },
    {
      title: "Клиент",
      render: (_, record) => {
        return (
          <>
            <FaUserTie size="1.5em" />
            <span> </span>
            <a
              onClick={() =>
                history.push(`/Clients/Info/${record.idClientAmbassador}`)
              }
            >
              {record.clientInfo}
            </a>
          </>
        );
      },
    },
    {
      title: "Cумма",
      dataIndex: "sum",
    },
    {
      title: "Действие",
      render: (_, record) => {
        if (record.dateСonfirmation == "Invalid date") {
          return <a onClick={() => acceptHandler(record)}>Подтвердить</a>;
        } else {
          return (
            <>
              <b>Подтверждено</b> <br /> {record.dateСonfirmation}
            </>
          );
        }
      },
    },
  ];

  return (
    <div className="site-card-border-less-wrapper">
      <h4>Список документов на вывод денег</h4>
      <Breadcrumb>
        <Breadcrumb.Item href="">
          <HomeOutlined />
        </Breadcrumb.Item>
        <Breadcrumb.Item>Список документов</Breadcrumb.Item>
      </Breadcrumb>
      <br />
      <Card
        loading={spinner}
        title="Документы"
        bordered={false}
        style={{ width: { winWidth } }}
      >
        <div className="left-right">
          <div>
            <Radio.Group
              optionType="button"
              buttonStyle="solid"
              onChange={(e) => modifyTable(e.target.value)}
            >
              <Radio.Button value="true">Confirmed</Radio.Button>
              <Radio.Button value="false">Not confirmed</Radio.Button>
            </Radio.Group>
          </div>
          <div></div>
        </div>
        <br />
        {docs && (
          <Table
            columns={columns}
            dataSource={docs}
            pagination={{ position: ["none", "bottomCenter"] }}
          />
        )}
      </Card>
    </div>
  );
};

export default PaymentList;
