import "./EnterpriseInfo.css";
import * as constant from "../constant";
import { Card, Row, Col, Button, Modal, Select } from "antd";
import { Breadcrumb, Input, Space, Table, message } from "antd";
import {
  HomeOutlined,
  ContactsTwoTone,
  TagTwoTone,
  SearchOutlined,
} from "@ant-design/icons";
import { Link } from "react-router-dom";
import { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import Highlighter from "react-highlight-words";
import copy from "copy-to-clipboard";
import EnterpriseJSONParser from "../EnterpriseJSONParser/EnterpriseJSONParser";
import EntityMediaData from "../EntityMediaData/EntityMediaData";
import EnterpriseCollaborateInfo from "../EnterpriseCollaborateInfo/EnterpriseCollaborateInfo";
import NomenclaturaCreate from "../NomenclaturaCreatre/NomenclaturaCreate";

const EnterpriseInfo = () => {
  const winWidth = window.innerWidth;
  const { enterprises } = useSelector((state) => state);
  const { roles } = useSelector((state) => state);
  const [spinner, setSpinner] = useState(false);
  const [item, setItem] = useState("");
  const [user, setUser] = useState("");
  const [uid, setCurrentUid] = useState("");
  const [name, setCurrentName] = useState("");
  const [modalVis, setModalVis] = useState(false);
  const [modalRole, setModalRole] = useState(false);
  const [newPass, setNewPass] = useState("");
  const [newRole, setNewRole] = useState("");
  const [enterpriseKey, setEnterpriseKey] = useState("");
  const { Option } = Select;
  const dispatch = useDispatch();

  // Достаем id из URL

  useEffect(() => {
    const parts = String(window.location).split("/");
    const uid = parts.pop() || parts.pop();
    getDataFromId(uid);
  }, []);

  // Находим в "базе" организаций элемент с данным id

  const getDataFromId = (id) => {
    const enterprise = enterprises.filter((elem) => elem.idUnique === id)[0];
    if (enterprise) {
      setItem(enterprise);
      dispatch({
        type: "SET_CURRENT_ENTERPRISE",
        payload: {
          enterpriseId: enterprise.idUnique,
          enterpriseName: enterprise.name,
        },
      });
      getUsers(enterprise.idUnique);
    }
  };

  // запрос на получение списка пользователей организации

  const getUsers = async (id) => {
    const token = localStorage.getItem("token");
    const api = constant.API;
    setSpinner(true);
    const response = await fetch(
      `${api}/api/enterprise/user/list?IDEnterprise=${id}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `bearer ${token}`,
          AccessKey: constant.ACCESSKEY,
        },
      }
    );
    if (response.status === 401 || response.status === 403) {
      return dispatch({ type: "LOGOUT" });
    }
    const result = await response.json();
    setSpinner(false);
    // проверка на корректность ответа
    if (result.result.status === 0) {
      console.log("FIND ROLES!!!->", result.dataList);
      setUser(result.dataList);
    } else message.error(result.message);
  };

  //  изменение пароля пользователя

  const changePassword = async (params) => {
    const { userId, username } = params;
    setModalVis(false);
    if (!newPass) return message.error("Необходимо ввести пароль");

    const token = localStorage.getItem("token");
    const api = constant.API;
    const response = await fetch(`${api}/api/enterprise/user/`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization: `bearer ${token}`,
        AccessKey: constant.ACCESSKEY,
      },
      body: JSON.stringify({
        idUser: userId,
        passwordDefault: newPass,
        name: username,
      }),
    });
    if (response.status === 401 || response.status === 403) {
      return dispatch({ type: "LOGOUT" });
    }
    const result = await response.json();

    // проверка на корректность ответа
    if (result.status === 0) {
      message.info("Пароль изменен успешно");
    } else message.error(result.message);
  };

  //  назначение роли пользователю

  const changeRole = async (params) => {
    const { userId, username } = params;
    setModalRole(false);
    if (!newRole) return message.error("Необходимо выбрать роль");

    const token = localStorage.getItem("token");
    const api = constant.API;
    const response = await fetch(
      `${api}/api/enterprise/user/${userId}/role/${newRole}`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `bearer ${token}`,
          AccessKey: constant.ACCESSKEY,
        },
        body: JSON.stringify({
          idUser: userId,
          passwordDefault: newPass,
          name: username,
        }),
      }
    );
    if (response.status === 401 || response.status === 403) {
      return dispatch({ type: "LOGOUT" });
    }
    const result = await response.json();

    // проверка на корректность ответа
    if (result.status === 0) {
      message.info("Роль успешно добавлена");
    } else message.error(result.message);
  };

  // запрос на генерацию ключа для организации

  const generateEnterpriseKeys = async () => {
    const token = localStorage.getItem("token");
    const api = constant.API;
    const response = await fetch(
      `${api}/api/apikey/generate/${item.idUnique}`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `bearer ${token}`,
          AccessKey: constant.ACCESSKEY,
        },
        body: JSON.stringify({ alias: "DevKey" }),
      }
    );
    if (response.status === 401 || response.status === 403) {
      return dispatch({ type: "LOGOUT" });
    }
    const result = await response.json();
    // проверка на корректность ответа
    if (result.result.status === 0) {
      setEnterpriseKey(result.data.key);
      copy(result.data.key);
      message.success("Ключ скопирован в буфер обмена");
    } else message.error(result.result.message);
  };

  // Поиск по колонке

  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  let searchInput;

  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node) => {
            searchInput = node;
          }}
          placeholder={`Введите запрос`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Поиск
          </Button>
          <Button
            onClick={() => handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Сброс
          </Button>
          <Button
            type="link"
            size="small"
            onClick={() => {
              confirm({ closeDropdown: false });
              setSearchText(selectedKeys[0]);
              setSearchedColumn(dataIndex);
            }}
          ></Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex]
            .toString()
            .toLowerCase()
            .includes(value.toLowerCase())
        : "",
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.select(), 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      ),
  });

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();

    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText("");
  };

  // Конец поиска по колонке

  // Обработчик перехода на изменение пароля

  const handleDetails = (params) => {
    setCurrentUid(params.id);
    setCurrentName(params.name);
    setModalVis(true);
  };

  // Обработчик перехода на добавление роли

  const handleRole = (params) => {
    setCurrentUid(params.id);
    setCurrentName(params.name);
    setModalRole(true);
  };

  // Описание столбцов

  const columns = [
    {
      title: "ID",
      dataIndex: "idUnique",
      ...getColumnSearchProps("idUnique"),
      sorter: {
        compare: (a, b) => a.key - b.key,
      },
      responsive: ["lg"],
    },
    {
      title: "Имя",
      dataIndex: "name",
      ...getColumnSearchProps("name"),
      sorter: {
        compare: (a, b) => a.key - b.key,
      },
    },
    {
      title: "E-mail",
      dataIndex: "email",
      ...getColumnSearchProps("email"),
      sorter: {
        compare: (a, b) => a.key - b.key,
      },
      responsive: ["lg"],
    },
    {
      title: "Действие",
      dataIndex: "",
      key: "x",
      render: (_, record) => (
        <>
          <a
            onClick={() =>
              handleDetails({ id: record.idUnique, name: record.name })
            }
          >
            Сменить пароль
          </a>
          <br />
          <a
            onClick={() =>
              handleRole({ id: record.idUnique, name: record.name })
            }
          >
            Добавить роль
          </a>
        </>
      ),
    },
  ];

  return (
    <div className="site-card-border-less-wrapper">
      <h4>Подробнее об организации</h4>
      <Breadcrumb>
        <Breadcrumb.Item href="">
          <HomeOutlined />
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <Link to="/Enterprises/List">Организации</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Подробнее об организации</Breadcrumb.Item>
      </Breadcrumb>
      <br />
      <Card
        loading={spinner}
        title="Основная информация"
        bordered={false}
        style={{ width: { winWidth } }}
      >
        <Row>
          <Col span={12}>
            <ContactsTwoTone style={{ fontSize: "20px" }} />
            <font size="3"> Название </font>
          </Col>
          <Col span={12}>
            <font size="4" color="#1890FF">
              <b>{item && item.name}</b>
            </font>
          </Col>
        </Row>
        <br />
        <Row>
          <Col span={12}>
            <TagTwoTone style={{ fontSize: "20px" }} />
            <font size="3"> Идентификатор</font>
          </Col>
          <Col span={12}>
            <font size="3" color="#1890FF">
              {item && item.idUnique}
            </font>
          </Col>
        </Row>
        <br />
        <EnterpriseCollaborateInfo uuid={item.idUnique} />
        <br />
        <NomenclaturaCreate uuid={item.idUnique} />
        <br />
        <Card title="Магазины" bordered={true} style={{ width: { winWidth } }}>
          <Button type="primary" size="large">
            <Link to="/Shops/List">Список магазинов</Link>
          </Button>
        </Card>
        <br />
        <EntityMediaData uuid={item.idUnique} />
        <Card
          title="Пользователи"
          bordered={true}
          style={{ width: { winWidth } }}
        >
          <Button type="primary" size="large">
            <Link to="/Users/New">Создать пользователя</Link>
          </Button>
          <br />
          <br />

          <Table
            columns={columns}
            // Пофиксить dataSource - все же попробывть вытянуть из useState
            dataSource={user}
            pagination={{ position: ["none", "bottomCenter"] }}
          />

          {/* Модалка для смены пароля */}
          <Modal
            title="Придумайте новый пароль"
            style={{ top: 20 }}
            visible={modalVis}
            onOk={() =>
              changePassword({
                userId: uid,
                username: name,
              })
            }
            onCancel={() => setModalVis(false)}
          >
            {" "}
            <p>Новый пароль</p>
            <Input
              type="password"
              onChange={(e) => setNewPass(e.target.value)}
            />
          </Modal>

          {/* Модалка для назначения роли */}
          <Modal
            title="Назначить роль"
            style={{ top: 20 }}
            visible={modalRole}
            onOk={() =>
              changeRole({
                userId: uid,
                username: name,
              })
            }
            onCancel={() => setModalRole(false)}
          >
            {" "}
            <p>Новая роль</p>
            <Select style={{ width: 160 }} onChange={(e) => setNewRole(e)}>
              {roles &&
                roles.map((el) => {
                  return (
                    <>
                      <Option value={el.idUnique}>{el.name}</Option>
                    </>
                  );
                })}
            </Select>
          </Modal>
        </Card>
        <Card title="Ключи" bordered={true} style={{ width: { winWidth } }}>
          <Button type="primary" size="large" onClick={generateEnterpriseKeys}>
            Копировать ключ
          </Button>
          <br />
          <br />
          {enterpriseKey && (
            <>
              {" "}
              Ключ: <b> {enterpriseKey}</b>
            </>
          )}
        </Card>
        <br />
        <EnterpriseJSONParser />
      </Card>
    </div>
  );
};

export default EnterpriseInfo;
