import "./ShopInfo.css";
import * as constant from "../constant";
import { Card, Row, Col } from "antd";
import { Breadcrumb, Image, message } from "antd";
import {
  HomeOutlined,
  ContactsTwoTone,
  TagTwoTone,
  CompassTwoTone,
  EnvironmentTwoTone,
  CameraTwoTone,
  CreditCardTwoTone,
} from "@ant-design/icons";
import { Link, useHistory } from "react-router-dom";
import { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import EntityMediaData from "../EntityMediaData/EntityMediaData";
import ShopProducts from "../ShopProducts/ShopProducts";

const ShopInfo = () => {
  const winWidth = window.innerWidth;
  const dispatch = useDispatch();
  const history = useHistory();
  const { currentEnterpriseId } = useSelector((state) => state);
  const { currentEnterpriseName } = useSelector((state) => state);
  const [spinner, setSpinner] = useState(false);
  const [shop, setShop] = useState("");

  // Достаем id из URL

  useEffect(() => {
    const parts = String(window.location).split("/");
    const uid = parts.pop() || parts.pop();
    getDataFromId(uid);
  }, []);

  // Находим в "базе" организаций элемент с данным id

  const getDataFromId = async (uid) => {
    const token = localStorage.getItem("token");
    const api = constant.APISHOP;
    setSpinner(true);
    const response = await fetch(
      `${api}/iecommercecore/api/v1/shop/enterprise/${currentEnterpriseId}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `bearer ${token}`,
          AccessKey: constant.ACCESSKEY,
        },
      }
    );
    setSpinner(false);
    if (response.status === 401 || response.status === 403) {
      return dispatch({ type: "LOGOUT" });
    }
    if (!response.ok) {
      dispatch({
        type: "SET_DEBUG_MESSAGE",
        payload: response.statusText,
      });
      return message.error("Произошла ошибка");
    }
    const result = await response.json();
    // проверка на корректность ответа
    if (result.result.status === 0) {
      const filteredShops = result.dataList.filter((el) => el.idUnique == uid);
      setShop(filteredShops[0]);
      dispatch({
        type: "SET_DEBUG_MESSAGE",
        payload: response.statusText,
      });
    } else message.error(result.result.message);
  };

  return (
    <div className="site-card-border-less-wrapper">
      <h4>Подробнее о магазине</h4>
      <Breadcrumb>
        <Breadcrumb.Item href="">
          <HomeOutlined />
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <Link to="/Enterprises/List">Организации</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <span>{currentEnterpriseName}</span>
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <Link to="/Shops/List">Список магазинов</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Подробнее</Breadcrumb.Item>
      </Breadcrumb>

      <br />
      <Card
        loading={spinner}
        title="Основная информация"
        bordered={false}
        style={{ width: { winWidth } }}
      >
        <Row>
          <Col span={12}>
            <ContactsTwoTone style={{ fontSize: "20px" }} />
            <font size="3"> Название </font>
          </Col>
          <Col span={12}>
            <font size="4" color="#1890FF">
              <b>{shop && shop.name}</b>
            </font>
          </Col>
        </Row>
        <br />
        <Row>
          <Col span={12}>
            <CameraTwoTone style={{ fontSize: "20px" }} />
            <font size="3"> Фото </font>
          </Col>
          <Col span={12}>
            <Image
              width={200}
              src={shop.imageURL}
              fallback="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg=="
              alt="image"
            />
          </Col>
        </Row>
        <br />
        <Row>
          <Col span={12}>
            <TagTwoTone style={{ fontSize: "20px" }} />
            <font size="3"> Идентификатор</font>
          </Col>
          <Col span={12}>
            <font size="3" color="#1890FF">
              {shop && shop.idUnique}
            </font>
          </Col>
        </Row>
        <br />
        <Row>
          <Col span={12}>
            <EnvironmentTwoTone style={{ fontSize: "20px" }} />
            <font size="3"> Адрес</font>
          </Col>
          <Col span={12}>
            <font size="3" color="#1890FF">
              {shop && shop.address}
            </font>
          </Col>
        </Row>
        <br />
        <Row>
          <Col span={12}>
            <CreditCardTwoTone style={{ fontSize: "20px" }} />
            <font size="3"> Контакты</font>
          </Col>
          <Col span={12}>
            <font size="3" color="#1890FF">
              {shop && shop.contacts}
            </font>
          </Col>
        </Row>
        <br />
        <Row>
          <Col span={12}>
            <CompassTwoTone style={{ fontSize: "20px" }} />
            <font size="3"> Широта</font>
          </Col>
          <Col span={12}>
            <font size="3" color="#1890FF">
              {shop && shop.latitude}
            </font>
          </Col>
        </Row>
        <br />
        <Row>
          <Col span={12}>
            <CompassTwoTone style={{ fontSize: "20px" }} />
            <font size="3"> Долгота</font>
          </Col>
          <Col span={12}>
            <font size="3" color="#1890FF">
              {shop && shop.longitude}
            </font>
          </Col>
        </Row>
        <br />
      </Card>
      <br />
      <ShopProducts uuid={shop.idUnique} />
      <br />
      <EntityMediaData uuid={shop.idUnique} />
    </div>
  );
};

export default ShopInfo;
