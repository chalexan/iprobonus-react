import "./UsersCreate.css";
import * as constant from "../constant";
import { Card, Form, Input, Button, Alert } from "antd";
import { Breadcrumb, message } from "antd";
import { HomeOutlined } from "@ant-design/icons";
import { useState } from "react";
import { useHistory, Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

const UsersCreate = () => {
  const winWidth = window.innerWidth;
  const history = useHistory();
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [emailErr, setEmailErr] = useState("");
  const [passwordErr, setPasswordErr] = useState("");
  const [usernameErr, setUsermnameErr] = useState("");
  const { currentEnterpriseId } = useSelector((state) => state);
  const { currentEnterpriseName } = useSelector((state) => state);
  const dispatch = useDispatch();

  //  создание нового пользователя

  const createUser = async () => {
    // проверки заполнения полей

    if (!username) return setUsermnameErr("Введите имя пользователя");
    if (!password) return setPasswordErr("Введите пароль");
    if (!email) return setEmailErr("Введите E-mail");
    setUsermnameErr("");
    setPasswordErr("");
    setEmailErr("");

    //запрос на добавление нового пользователя

    const token = localStorage.getItem("token");
    const api = constant.API;

    const response = await fetch(`${api}/api/enterprise/user/`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `bearer ${token}`,
        AccessKey: constant.ACCESSKEY,
      },
      body: JSON.stringify({
        idEnterprise: currentEnterpriseId,
        email: email,
        passwordDefault: password,
        name: username,
      }),
    });
    if (response.status === 401) {
      return dispatch({ type: "LOGOUT" });
    }
    if (!response.ok) {
      dispatch({
        type: "SET_DEBUG_MESSAGE",
        payload: response.statusText,
      });
      return setUsermnameErr("Произошла ошибка");
    }
    const result = await response.json();
    // проверка на корректность ответа
    if (result.status === 0) {
      message.success("Пользователь создан");
      dispatch({
        type: "SET_DEBUG_MESSAGE",
        payload: response.statusText,
      });
      history.goBack();
    } else message.error(result.message);
  };

  return (
    <div className="site-card-border-less-wrapper">
      <h4>Cоздать пользователя</h4>
      <Breadcrumb>
        <Breadcrumb.Item href="">
          <HomeOutlined />
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <Link to="/Enterprises/List">Организации</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <span>{currentEnterpriseName}</span>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Создать пользователя</Breadcrumb.Item>
      </Breadcrumb>
      <br />

      <Card bordered={false} style={{ width: { winWidth } }}>
        <Form layout="vertical">
          <Form.Item label="Организация:  ">
            <div className="flex-container">
              <b>
                {currentEnterpriseName}({currentEnterpriseId})
              </b>
            </div>
          </Form.Item>
        </Form>
        {currentEnterpriseId ? (
          <Form layout="vertical">
            <Form.Item label="Имя пользователя">
              <Input
                onChange={(e) => setUsername(e.target.value)}
                size="large"
                type="text"
              />

              {usernameErr ? (
                <>
                  <br />
                  <br />
                  <Alert message={usernameErr} type="error" />
                </>
              ) : null}
            </Form.Item>
            <Form.Item label="E-mail">
              <Input
                onChange={(e) => setEmail(e.target.value)}
                size="large"
                type="email"
              />
              <br />
              {emailErr ? (
                <>
                  <br /> <Alert message={emailErr} type="error" />{" "}
                </>
              ) : null}
            </Form.Item>
            <Form.Item label="Пароль">
              <Input
                onChange={(e) => setPassword(e.target.value)}
                size="large"
                type="password"
              />
              <br />
              {passwordErr ? (
                <>
                  <br />
                  <Alert message={passwordErr} type="error" />{" "}
                </>
              ) : null}
            </Form.Item>
            <Form.Item>
              <Button type="primary" onClick={createUser} size="large">
                Зарегистрировать
              </Button>
            </Form.Item>
          </Form>
        ) : null}
      </Card>
    </div>
  );
};

export default UsersCreate;
