import "./NomenclaturaCreate.css";
import * as constant from "../constant";
import { Card, message, Row, Col } from "antd";
import { Input, Button, Switch, Checkbox } from "antd";
import { CheckOutlined, CloseOutlined } from "@ant-design/icons";
import { useState, useEffect } from "react";
import { useDispatch } from "react-redux";

const NomenclaturaCreate = (props) => {
  const winWidth = window.innerWidth;
  const dispatch = useDispatch();

  const [addNomenclature, setAddNomenclature] = useState(false);

  //  стейты для формы

  const [code, setCode] = useState("null");
  const [name, setName] = useState("null");
  const [artikul, setArticul] = useState("null");
  const [vat, setVat] = useState(1);
  const [typeUnitOfMeasurement, setTypeUnitOfMeasurement] = useState(0);
  const [valueUnitOfMeasurement, setValueUnitOfMeasurement] = useState(0);
  const [measurementDescription, setMeasurementDescription] = useState("null");
  const [showComposition, setShowComposition] = useState(false);
  const [barcode, setBarcode] = useState("null");
  const [imagesJSON, setImagesJSON] = useState("null");
  const [commerseDescription, setCommerseDescription] = useState("null");
  const [nameFeature, setNameFeature] = useState("null");
  const [additionaInfo, setAdditionalInfo] = useState("null");
  const [additionaInfoJSON, setAdditionaInfoJSON] = useState("null");

  //  стейты с idшниками откуда взять - не знаю

  const [idCategory, setIdCategory] = useState(
    "3fa85f64-5717-4562-b3fc-2c963f66afa6"
  );
  const [idTypeProduct, setIdTypeProduct] = useState(
    "3fa85f64-5717-4562-b3fc-2c963f66afa6"
  );
  const [idFeature, setIdFeature] = useState(
    "3fa85f64-5717-4562-b3fc-2c963f66afa6"
  );
  const [idrfProducingCountry, setIdrfProducingCountry] = useState(
    "3fa85f64-5717-4562-b3fc-2c963f66afa6"
  );
  const [manufacturer, setManufacturer] = useState(
    "3fa85f64-5717-4562-b3fc-2c963f66afa6"
  );

  // функция создания новой номенклатуры

  const submitHandler = async () => {
    const api = constant.APISHOP;
    const accessKey = constant.ACCESSKEY;

    const response = await fetch(
      `${api}/iecommercecore/api/v1/products/nomenclature`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          AccessKey: accessKey,
        },
        body: JSON.stringify({
          idEnterprise: props.uuid,
          code,
          name,
          artikul,
          idCategory,
          idTypeProduct,
          vat,
          typeUnitOfMeasurement,
          valueUnitOfMeasurement,
          measurementDescription,
          showComposition,
          barcode,
          imagesJSON,
          commerseDescription,
          idFeature,
          nameFeature,
          additionaInfo,
          additionaInfoJSON,
          idrfProducingCountry,
          manufacturer,
        }),
      }
    );

    if (response.status === 401 || response.status === 403) {
      return dispatch({ type: "LOGOUT" });
    }

    if (!response.ok) {
      message.error("Ошибка при создании");
      return dispatch({
        type: "SET_DEBUG_MESSAGE",
        payload: response.statusText,
      });
    }

    const result = await response.json();
    message.info("Данные сохранены");
    console.log("responce", result);
  };

  return (
    <Card
      title="Добавить номенклатуру товара"
      bordered={true}
      style={{ width: { winWidth } }}
    >
      <b>uuid: {props.uuid} </b>
      <br />
      <br />
      <Checkbox
        onChange={(e) =>
          e.target.checked
            ? setAddNomenclature(true)
            : setAddNomenclature(false)
        }
      >
        Добавить номенклатуру
      </Checkbox>
      <br />
      <br />
      {addNomenclature && (
        <Card
          title="Создание новой номенклатуры"
          bordered={false}
          style={{ width: { winWidth } }}
        >
          <Row>
            <Col span={12}>
              <font size="3"> Code </font>
            </Col>
            <Col span={12}>
              <Input onChange={(e) => setCode(e.target.value)} type="text" />
            </Col>
          </Row>
          <br />
          <Row>
            <Col span={12}>
              <font size="3"> Name </font>
            </Col>
            <Col span={12}>
              <Input onChange={(e) => setName(e.target.value)} type="text" />
            </Col>
          </Row>
          <br />
          <Row>
            <Col span={12}>
              <font size="3"> Articul </font>
            </Col>
            <Col span={12}>
              <Input onChange={(e) => setArticul(e.target.value)} type="text" />
            </Col>
          </Row>
          <br />
          <Row>
            <Col span={12}>
              <font size="3"> VAT </font>
            </Col>
            <Col span={12}>
              <Input
                onChange={(e) => setVat(e.target.value)}
                type="text"
                defaultValue={1}
              />
            </Col>
          </Row>
          <br />
          <Row>
            <Col span={12}>
              <font size="3"> TypeUnitOfMeasurement </font>
            </Col>
            <Col span={12}>
              <Input
                onChange={(e) => setTypeUnitOfMeasurement(e.target.value)}
                type="text"
                defaultValue={0}
              />
            </Col>
          </Row>
          <br />
          <Row>
            <Col span={12}>
              <font size="3"> ValueUnitOfMeasurement </font>
            </Col>
            <Col span={12}>
              <Input
                onChange={(e) => setValueUnitOfMeasurement(e.target.value)}
                type="text"
                defaultValue={0}
              />
            </Col>
          </Row>
          <br />
          <Row>
            <Col span={12}>
              <font size="3"> MeasurementDescription </font>
            </Col>
            <Col span={12}>
              <Input
                onChange={(e) => setMeasurementDescription(e.target.value)}
                type="text"
              />
            </Col>
          </Row>
          <br />
          <Row>
            <Col span={12}>
              <font size="3"> ShowComposition </font>
            </Col>
            <Col span={12}>
              <Switch
                checkedChildren={<CheckOutlined />}
                unCheckedChildren={<CloseOutlined />}
                defaultChecked={false}
                onChange={(e) => setShowComposition(e)}
                size="default"
              />
            </Col>
          </Row>
          <br />
          <Row>
            <Col span={12}>
              <font size="3"> Barcode </font>
            </Col>
            <Col span={12}>
              <Input onChange={(e) => setBarcode(e.target.value)} type="text" />
            </Col>
          </Row>
          <br />
          <Row>
            <Col span={12}>
              <font size="3"> imagesJSON </font>
            </Col>
            <Col span={12}>
              <Input
                onChange={(e) => setImagesJSON(e.target.value)}
                type="text"
              />
            </Col>
          </Row>
          <br />
          <Row>
            <Col span={12}>
              <font size="3"> CommerseDescription </font>
            </Col>
            <Col span={12}>
              <Input
                onChange={(e) => setCommerseDescription(e.target.value)}
                type="text"
              />
            </Col>
          </Row>
          <br />
          <Row>
            <Col span={12}>
              <font size="3"> NameFeature </font>
            </Col>
            <Col span={12}>
              <Input
                onChange={(e) => setNameFeature(e.target.value)}
                type="text"
              />
            </Col>
          </Row>
          <br />
          <Row>
            <Col span={12}>
              <font size="3"> AdditionalInfo </font>
            </Col>
            <Col span={12}>
              <Input
                onChange={(e) => setAdditionalInfo(e.target.value)}
                type="text"
              />
            </Col>
          </Row>
          <br />
          <Row>
            <Col span={12}>
              <font size="3"> AdditionaInfoJSON </font>
            </Col>
            <Col span={12}>
              <Input
                onChange={(e) => setAdditionaInfoJSON(e.target.value)}
                type="text"
              />
            </Col>
          </Row>
          <br />
          <Button size="large" type="primary" onClick={submitHandler}>
            Сохранить
          </Button>
        </Card>
      )}
    </Card>
  );
};

export default NomenclaturaCreate;
