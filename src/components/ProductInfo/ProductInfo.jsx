import "./ProductInfo.css";
import * as constant from "../constant";
import { Card, Row, Col } from "antd";
import { Breadcrumb, Image, message, Input, Button } from "antd";
import {
  HomeOutlined,
  ContactsTwoTone,
  TagTwoTone,
  CompassTwoTone,
  EnvironmentTwoTone,
  CameraTwoTone,
  CreditCardTwoTone,
  PropertySafetyFilled,
} from "@ant-design/icons";
import { Link, useHistory } from "react-router-dom";
import { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { CheckOutlined, CloseOutlined } from "@ant-design/icons";
import moment from "moment";
const ProductInfo = () => {
  const winWidth = window.innerWidth;
  const dispatch = useDispatch();
  const history = useHistory();
  const { currentEnterpriseId } = useSelector((state) => state);
  const { currentEnterpriseName } = useSelector((state) => state);
  const { currentShopId } = useSelector((state) => state);
  const { currentNomenklatura } = useSelector((state) => state);
  const [productData, setProductData] = useState(null);
  const [spinner, setSpinner] = useState(false);
  const [goodsId, setGoodsId] = useState("null");

  //стейты формы

  const [name, setName] = useState("null");
  const [articul, setArticul] = useState("null");
  const [barcode, setBarcode] = useState("null");
  const [nameFeature, setNameFeature] = useState("null");
  const [additionalInfo, setAdditionalInfo] = useState("null");
  const [extendedDescription, setExtendedDescription] = useState("null");
  const [url, setURL] = useState("null");
  const [popularOrder, setPopularOrder] = useState(0);
  const [rating, setRating] = useState(0);
  const [defectName, setDefectName] = useState("null");
  const [quantity, setQuantity] = useState(0);
  const [beginPrice, setBeginPrice] = useState(0);
  const [currentPrice, setCurrentPrice] = useState(0);
  const [costPrice, setCostPrice] = useState(0);
  const [costtPriceBasicUnit, setCosttPriceBasicUnit] = useState(0);
  const [maxValueDiscount, setMaxValueDiscount] = useState(0);
  const [lastTimeUpdate, setLastTimeUpdate] = useState("null");

  // Достаем id из URL

  useEffect(() => {
    const parts = String(window.location).split("/");
    const uid = parts.pop() || parts.pop();
    getDataFromId(uid);
    setGoodsId(uid);
  }, [currentEnterpriseId]);

  // Находим в товар по id и выводим данные в карточку

  const getDataFromId = async (uid) => {
    const token = localStorage.getItem("token");
    const api = constant.APISHOP;
    setSpinner(true);
    const response = await fetch(
      `${api}/iecommercecore/api/v1/products/product/${uid}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `bearer ${token}`,
          AccessKey: constant.ACCESSKEY,
        },
      }
    );
    setSpinner(false);
    if (response.status === 401) {
      return dispatch({ type: "LOGOUT" });
    }
    if (!response.ok) {
      dispatch({
        type: "SET_DEBUG_MESSAGE",
        payload: response.statusText,
      });
      return message.error("Произошла ошибка");
    }
    const result = await response.json();
    console.log("productData", result.data.listProducts[0]);
    setProductData(result.data.listProducts[0]);
    // установить все стейты
    setLastTimeUpdate(result.data.listProducts[0].lastTimeUpdate);
    setMaxValueDiscount(result.data.listProducts[0].maxValueDiscount);
    setCosttPriceBasicUnit(result.data.listProducts[0].costtPriceBasicUnit);
    setCostPrice(result.data.listProducts[0].costPrice);
    setCurrentPrice(result.data.listProducts[0].currentPrice);
    setBeginPrice(result.data.listProducts[0].beginPrice);
    setQuantity(result.data.listProducts[0].quantity);
    setDefectName(result.data.listProducts[0].defectName);
    setRating(result.data.listProducts[0].rating);
    setPopularOrder(result.data.listProducts[0].popularOrder);
    setURL(result.data.listProducts[0].url);
    setExtendedDescription(result.data.listProducts[0].extendedDescription);
    setAdditionalInfo(result.data.listProducts[0].additionaInfo);
    setNameFeature(result.data.listProducts[0].nameFeature);
    setBarcode(result.data.listProducts[0].barcode);
    setArticul(result.data.listProducts[0].artikul);
    setName(result.data.listProducts[0].name);
  };

  // запрос на обновление данных о товаре (PUT)

  const updateHandler = async () => {
    const token = localStorage.getItem("token");
    const api = constant.APISHOP;
    setSpinner(true);
    const response = await fetch(
      `${api}/iecommercecore/api/v1/products/product/`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `bearer ${token}`,
          AccessKey: constant.ACCESSKEY,
        },
        body: JSON.stringify({
          idUnique: goodsId,
          name,
          extendedDescription,
          artikul: articul,
          idEnterprise: currentEnterpriseId,
          url,
          imageDataJSON: "string",
          additionalDataJSON: "string",
          idCategory: "3fa85f64-5717-4562-b3fc-2c963f66afa6",
          popularOrder,
          rating,
          barcode,
          idDiscountBasisForBeginPrice: "3fa85f64-5717-4562-b3fc-2c963f66afa6",
          idFeature: "3fa85f64-5717-4562-b3fc-2c963f66afa6",
          nameFeature,
          defectName,
          kiz: "string",
          additionaInfo: additionalInfo,
          lastTimeUpdate: moment(),
          idrfNomenclatura: currentNomenklatura,
          idrfShop: currentShopId,
          quantity,
          beginPrice,
          currentPrice,
          costPrice,
          costtPriceBasicUnit,
          maxValueDiscount,
          idrfPlace: "3fa85f64-5717-4562-b3fc-2c963f66afa6",
        }),
      }
    );
    setSpinner(false);
    if (response.status === 401 || response.status === 403) {
      return dispatch({ type: "LOGOUT" });
    }
    if (!response.ok) {
      dispatch({
        type: "SET_DEBUG_MESSAGE",
        payload: response.statusText,
      });
      return message.error("Произошла ошибка");
    }
    const result = await response.json();
    console.log("productDataSaved", result);
    message.success("Данные сохранены");
  };

  return (
    <div className="site-card-border-less-wrapper">
      <h4>Подробнее о товаре</h4>
      <Breadcrumb>
        <Breadcrumb.Item href="">
          <HomeOutlined />
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <Link to="/Enterprises/List">Организации</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <span>{currentEnterpriseName}</span>
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <Link to="/Shops/List">Список магазинов</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Товар</Breadcrumb.Item>
        <Breadcrumb.Item>{productData && productData.name}</Breadcrumb.Item>
      </Breadcrumb>

      <br />
      <Card
        loading={spinner}
        title="Основная информация"
        bordered={false}
        style={{ width: { winWidth } }}
      >
        {productData && (
          <>
            <Row>
              <Col span={12}>
                <font size="3"> Название (Name) </font>
              </Col>
              <Col span={12}>
                <Input
                  onChange={(e) => setName(e.target.value)}
                  defaultValue={productData.name}
                  type="text"
                />
              </Col>
            </Row>
            <br />
            <Row>
              <Col span={12}>
                <font size="3"> Артикул (Articul) </font>
              </Col>
              <Col span={12}>
                <Input
                  onChange={(e) => setArticul(e.target.value)}
                  type="text"
                  defaultValue={productData.artikul}
                />
              </Col>
            </Row>
            <br />
            <Row>
              <Col span={12}>
                <font size="3"> Штрих-код (Barcode) </font>
              </Col>
              <Col span={12}>
                <Input
                  onChange={(e) => setBarcode(e.target.value)}
                  type="text"
                  defaultValue={productData.barcode}
                />
              </Col>
            </Row>
            <br />
            <Row>
              <Col span={12}>
                <font size="3"> Название фичи (NameFeature) </font>
              </Col>
              <Col span={12}>
                <Input
                  onChange={(e) => setNameFeature(e.target.value)}
                  type="text"
                  defaultValue={productData.nameFeature}
                />
              </Col>
            </Row>
            <br />
            <Row>
              <Col span={12}>
                <font size="3">
                  {" "}
                  Дополнительная информация (AdditionalInfo){" "}
                </font>
              </Col>
              <Col span={12}>
                <Input
                  onChange={(e) => setAdditionalInfo(e.target.value)}
                  type="text"
                  defaultValue={productData.additionaInfo}
                />
              </Col>
            </Row>
            <br />
            <Row>
              <Col span={12}>
                <font size="3">
                  {" "}
                  Расширенное описание (ExtendedDescription){" "}
                </font>
              </Col>
              <Col span={12}>
                <Input
                  onChange={(e) => setExtendedDescription(e.target.value)}
                  type="text"
                  defaultValue={productData.extendedDescription}
                />
              </Col>
            </Row>
            <br />
            <Row>
              <Col span={12}>
                <font size="3"> URL </font>
              </Col>
              <Col span={12}>
                <Input
                  onChange={(e) => setURL(e.target.value)}
                  type="url"
                  defaultValue={productData.url}
                />
              </Col>
            </Row>
            <br />
            <Row>
              <Col span={12}>
                <font size="3"> Порядок популярности (PopularOrder) </font>
              </Col>
              <Col span={12}>
                <Input
                  onChange={(e) => setPopularOrder(e.target.value)}
                  type="number"
                  defaultValue={productData.popularOrder}
                />
              </Col>
            </Row>
            <br />
            <Row>
              <Col span={12}>
                <font size="3"> Рейтинг (Rating) </font>
              </Col>
              <Col span={12}>
                <Input
                  onChange={(e) => setRating(e.target.value)}
                  type="number"
                  defaultValue={productData.rating}
                />
              </Col>
            </Row>
            <br />
            <Row>
              <Col span={12}>
                <font size="3"> Название дефекта (DefectName) </font>
              </Col>
              <Col span={12}>
                <Input
                  onChange={(e) => setDefectName(e.target.value)}
                  type="text"
                  defaultValue={productData.defectName}
                />
              </Col>
            </Row>
            <br />
            <Row>
              <Col span={12}>
                <font size="3"> Количество (Quantity) </font>
              </Col>
              <Col span={12}>
                <Input
                  onChange={(e) => setQuantity(Number(e.target.value))}
                  type="number"
                  defaultValue={productData.quantity}
                />
              </Col>
            </Row>
            <br />
            <Row>
              <Col span={12}>
                <font size="3"> Начальная цена (BeginPrice) </font>
              </Col>
              <Col span={12}>
                <Input
                  onChange={(e) => setBeginPrice(Number(e.target.value))}
                  type="number"
                  defaultValue={productData.beginPrice}
                />
              </Col>
            </Row>
            <br />
            <Row>
              <Col span={12}>
                <font size="3"> Текущая цена (CurrentPrice) </font>
              </Col>
              <Col span={12}>
                <Input
                  onChange={(e) => setCurrentPrice(Number(e.target.value))}
                  type="number"
                  defaultValue={productData.currentPrice}
                />
              </Col>
            </Row>
            <br />
            <Row>
              <Col span={12}>
                <font size="3"> Себестоимость (CostPrice) </font>
              </Col>
              <Col span={12}>
                <Input
                  onChange={(e) => setCostPrice(Number(e.target.value))}
                  type="number"
                  defaultValue={productData.costPrice}
                />
              </Col>
            </Row>
            <br />
            <Row>
              <Col span={12}>
                <font size="3">
                  {" "}
                  Базовая единица себестоимости (CosttPriceBasicUnit){" "}
                </font>
              </Col>
              <Col span={12}>
                <Input
                  onChange={(e) =>
                    setCosttPriceBasicUnit(Number(e.target.value))
                  }
                  type="number"
                  defaultValue={productData.costtPriceBasicUnit}
                />
              </Col>
            </Row>
            <br />
            <Row>
              <Col span={12}>
                <font size="3"> Максимальная скидка (MaxValueDiscount) </font>
              </Col>
              <Col span={12}>
                <Input
                  onChange={(e) => setMaxValueDiscount(Number(e.target.value))}
                  type="number"
                  defaultValue={productData.maxValueDiscount}
                />
              </Col>
            </Row>
            <br />
            <Row>
              <Col span={12}>
                <font size="3">
                  {" "}
                  Последнее время обнавления (lastTimeUpdate){" "}
                </font>
              </Col>
              <Col span={12}>
                <Input
                  onChange={(e) => setLastTimeUpdate(e.target.value)}
                  type="string"
                  defaultValue={productData.lastTimeUpdate}
                />
              </Col>
            </Row>
            <br />
            <Button size="large" type="primary" onClick={updateHandler}>
              Сохранить
            </Button>
          </>
        )}
      </Card>
    </div>
  );
};

export default ProductInfo;
