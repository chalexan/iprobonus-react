import "./ShopProducts.css";
import * as constant from "../constant";
import { Card, message, Row, Col, Table, Divider } from "antd";
import { Input, Button, Switch, Checkbox, Select } from "antd";
import { CheckOutlined, CloseOutlined } from "@ant-design/icons";
import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import moment from "moment";

const ShopProducts = (props) => {
  const winWidth = window.innerWidth;
  const dispatch = useDispatch();
  const { Option } = Select;
  const { currentEnterpriseId } = useSelector((state) => state);
  const history = useHistory();

  const [addGoods, setAddGoods] = useState(false);
  const [showGoods, setShowGoods] = useState(false);
  const [formFields, showFormFields] = useState(false);
  const [goodsData, setGoodsData] = useState("");
  const [nomenclatureData, setNomenclatureData] = useState("");
  const [selectedNomenclatureItem, setSelectedNomenclatureItem] = useState("");
  const [nomenclature, setNomenclature] = useState(null);
  const [nomenclatureGoods, setNomenclatureGoods] = useState(null);

  //  стейты с idшниками откуда взять - пока заглушки

  const [idCategory, setIdCategory] = useState(
    "3fa85f64-5717-4562-b3fc-2c963f66afa6" // из номенклатуры
  );

  const [idDiscountBasisForBeginPrice, setIdDiscountBasisForBeginPrice] =
    useState("3fa85f64-5717-4562-b3fc-2c963f66afa6");

  const [idFeature, setIdFeature] = useState(
    "3fa85f64-5717-4562-b3fc-2c963f66afa6" // из номенклатуры
  );

  const [idrfProducingCountry, setIdrfProducingCountry] = useState(
    "3fa85f64-5717-4562-b3fc-2c963f66afa6"
  );

  const [idrfPlace, setIdrfPlace] = useState(
    "3fa85f64-5717-4562-b3fc-2c963f66afa6"
  );

  //  стейты для формы - создание товара

  const [extendedDescription, setExtendedDescription] = useState("null");
  const [url, setUrl] = useState("null");
  const [popularOrder, setPopularOrder] = useState(0);
  const [rating, setRating] = useState(0);
  const [defectName, setDefectName] = useState("null");
  const [quantity, setQuantity] = useState(0);
  const [beginPrice, setBeginPrice] = useState(0);
  const [currentPrice, setCurrentPrice] = useState(0);
  const [costPrice, setCostPrice] = useState(0);
  const [costtPriceBasicUnit, setCosttPriceBasicUnit] = useState(0);
  const [maxValueDiscount, setMaxValueDiscount] = useState(0);
  const [kiz, setKiz] = useState("null");

  //  стейты для формы (номенклатура) -

  const [code, setCode] = useState("null");
  const [name, setName] = useState("null");
  const [artikul, setArticul] = useState("null");
  const [vat, setVat] = useState(1);
  const [typeUnitOfMeasurement, setTypeUnitOfMeasurement] = useState(0);
  const [valueUnitOfMeasurement, setValueUnitOfMeasurement] = useState(0);
  const [measurementDescription, setMeasurementDescription] = useState("null");
  const [showComposition, setShowComposition] = useState(false);
  const [barcode, setBarcode] = useState("null");
  const [imagesJSON, setImagesJSON] = useState("null");
  const [commerseDescription, setCommerseDescription] = useState("null");
  const [nameFeature, setNameFeature] = useState("null");
  const [additionaInfo, setAdditionalInfo] = useState("null");
  const [additionaInfoJSON, setAdditionaInfoJSON] = useState("null");

  useEffect(() => {
    getNomenclatures();
  }, []);

  // функция получения всех номенклатур

  const getNomenclatures = async () => {
    const token = localStorage.getItem("token");
    const api = constant.APISHOP;

    const response = await fetch(
      `${api}/iecommercecore/api/v1/products/nomenclature/list`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `bearer ${token}`,
        },
      }
    );
    if (response.status === 401 || response.status === 403) {
      return dispatch({ type: "LOGOUT" });
    }

    if (!response.ok) {
      message.error("Ошибка получения");
      return dispatch({
        type: "SET_DEBUG_MESSAGE",
        payload: response.statusText,
      });
    }

    const result = await response.json();
    console.log("responce", result.dataList);
    setNomenclatureData(result.dataList);
    setNomenclature(result.dataList[0].idUnique);
    setNomenclatureGoods(result.dataList[0].idUnique);
    setSelectedNomenclatureItem(result.dataList[0]);
  };

  // сохранение всех стейтов с текущей формы полей Номенклатура

  const setAllNomenclatureStates = (data) => {
    setCode(data.code);
    setName(data.name);
    setArticul(data.artikul);
    setVat(data.vat);
    setTypeUnitOfMeasurement(data.typeUnitOfMeasurement);
    setValueUnitOfMeasurement(data.valueUnitOfMeasurement);
    setMeasurementDescription(data.measurementDescription);
    setShowComposition(data.showComposition);
    setBarcode(data.barcode);
    setImagesJSON(data.imagesJSON);
    setCommerseDescription(data.commerseDescription);
    setNameFeature(data.nameFeature);
    setAdditionalInfo(data.additionaInfo);
    setAdditionaInfoJSON(data.additionaInfoJSON);
  };

  // Реакция на кнопку "продолжить" при просмотре товаров товара
  const showGoodsHandler = async () => {
    // Запрос к товару по номенклатуре и магазину
    const api = constant.APISHOP;
    const accessKey = constant.ACCESSKEY;
    dispatch({ type: "SET_CURRENT_SHOP_ID", payload: props.uuid });
    dispatch({ type: "SET_CURRENT_NOMENKLATURA", payload: nomenclatureGoods });
    const response = await fetch(
      `${api}/iecommercecore/api/v1/products/product/${nomenclatureGoods}/${props.uuid}`,
      {
        headers: {
          "Content-Type": "application/json",
          AccessKey: accessKey,
        },
      }
    );
    if (!response.ok) {
      message.error(`Ошибка: ${response.statusText}`);
      return dispatch({
        type: "SET_DEBUG_MESSAGE",
        payload: response.statusText,
      });
    }

    const result = await response.json();

    if (result.data.listProducts.length === 0) {
      return message.warning(`Товары не найдены`);
    }
    setGoodsData(result.data.listProducts);

    console.log("responce", result.data.listProducts.length);
  };

  // добавление нового товара
  const submitHandler = async () => {
    const api = constant.APISHOP;
    const accessKey = constant.ACCESSKEY;

    const response = await fetch(
      `${api}/iecommercecore/api/v1/products/product`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          AccessKey: accessKey,
        },
        body: JSON.stringify({
          idEnterprise: currentEnterpriseId,
          idrfNomenclatura: nomenclature,
          idrfShop: props.uuid,
          url,
          name,
          artikul,
          idCategory,
          kiz,
          popularOrder,
          rating,
          barcode,
          idDiscountBasisForBeginPrice,
          commerseDescription,
          idFeature,
          nameFeature,
          additionaInfo,
          defectName,
          quantity,
          beginPrice,
          currentPrice,
          costPrice,
          costtPriceBasicUnit,
          maxValueDiscount,
          idrfPlace,
          extendedDescription,
          lastTimeUpdate: moment(),
        }),
      }
    );
    console.log(".......>>>>", name);
    if (!response.ok) {
      message.error("Ошибка загрузки");
      return dispatch({
        type: "SET_DEBUG_MESSAGE",
        payload: response.statusText,
      });
    }

    const result = await response.json();
    message.info("Данные сохранены");
    console.log("responce", result);
  };

  const columns = [
    {
      title: "Дата добавления",
      render: (_, record) => {
        return <b>{record.lastTimeUpdate}</b>;
      },
    },
    {
      title: "Название",
      dataIndex: "name",
    },
    {
      title: "Артикул",
      dataIndex: "artikul",
    },
    {
      title: "Действие",
      render: (_, record) => {
        return (
          <a onClick={() => history.push(`/Product/Info/${record.idUnique}`)}>
            Подробнее
          </a>
        );
      },
    },
  ];

  return (
    <Card
      title="Товары магазина"
      bordered={true}
      style={{ width: { winWidth } }}
    >
      <b>uuid: {props.uuid} </b>
      <br />
      <br />
      <Checkbox
        onChange={(e) =>
          e.target.checked ? setAddGoods(true) : setAddGoods(false)
        }
      >
        Добавить товар
      </Checkbox>
      <Checkbox
        onChange={(e) =>
          e.target.checked ? setShowGoods(true) : setShowGoods(false)
        }
      >
        Cписок товаров магазина
      </Checkbox>
      <br />
      <br />

      {/* Если выбрана галлочка - список товаров  */}

      {showGoods && (
        <Card
          title="Товары магазина"
          bordered={false}
          style={{ width: { winWidth } }}
        >
          <b> Номенклатура </b>
          <br />
          <br />
          {nomenclatureData && (
            <>
              <Select
                defaultValue={nomenclatureData[0].idUnique}
                // style={{ width: 100 }}
                onChange={(e) => {
                  setNomenclature(e);
                  setNomenclatureGoods(e);
                  dispatch({
                    type: "SET_CURRENT_SHOP_ID",
                    payload: props.uuid,
                  });
                  dispatch({ type: "SET_CURRENT_NOMENKLATURA", payload: e });
                }}
              >
                {nomenclatureData.map((el) => {
                  return <Option value={el.idUnique}>{el.name}</Option>;
                })}
              </Select>

              <br />
              <br />
              <Button size="large" type="primary" onClick={showGoodsHandler}>
                Продолжить
              </Button>
              <br />
              <br />
            </>
          )}
          {goodsData && (
            <Table
              columns={columns}
              dataSource={goodsData}
              pagination={{ position: ["none", "bottomCenter"] }}
            />
          )}
        </Card>
      )}

      {/* Если выбрана галлочка - добавить товар  */}

      {addGoods && (
        <Card
          title="Добавление нового товара"
          bordered={false}
          style={{ width: { winWidth } }}
        >
          <b> Номенклатура </b>
          <br />
          <br />
          {nomenclatureData && (
            <>
              <Select
                defaultValue={nomenclatureData[0].idUnique}
                // style={{ width: 100 }}
                onChange={(e) => {
                  setNomenclature(e);
                  setNomenclatureGoods(e);
                  setSelectedNomenclatureItem(
                    nomenclatureData.filter((el) => el.idUnique == e)[0]
                  );
                  setAllNomenclatureStates(
                    nomenclatureData.filter((el) => el.idUnique == e)[0]
                  );
                  dispatch({
                    type: "SET_CURRENT_SHOP_ID",
                    payload: props.uuid,
                  });
                  dispatch({ type: "SET_CURRENT_NOMENKLATURA", payload: e });
                }}
              >
                {nomenclatureData.map((el) => {
                  return <Option value={el.idUnique}>{el.name}</Option>;
                })}
              </Select>

              <br />
              <br />
            </>
          )}

          <br />
          {/* Пользователь выбрал номенклатуру */}

          {selectedNomenclatureItem && (
            <>
              <font size="3">
                <b> Данные номенклатуры {nomenclature} </b>
              </font>
              <br />
              <Divider />
              <Row>
                <Col span={12}>
                  <font size="3"> Code </font>
                </Col>
                <Col span={12}>
                  <Input
                    type="text"
                    value={String(selectedNomenclatureItem.code)}
                  />
                </Col>
              </Row>
              <br />
              <Row>
                <Col span={12}>
                  <font size="3"> Name </font>
                </Col>
                <Col span={12}>
                  <Input
                    type="text"
                    value={String(selectedNomenclatureItem.name)}
                  />
                </Col>
              </Row>
              <br />
              <Row>
                <Col span={12}>
                  <font size="3"> Articul </font>
                </Col>
                <Col span={12}>
                  <Input
                    type="text"
                    value={String(selectedNomenclatureItem.artikul)}
                  />
                </Col>
              </Row>
              <br />
              <Row>
                <Col span={12}>
                  <font size="3"> VAT </font>
                </Col>
                <Col span={12}>
                  <Input
                    type="text"
                    value={String(selectedNomenclatureItem.vat)}
                  />
                </Col>
              </Row>
              <br />
              <Row>
                <Col span={12}>
                  <font size="3"> TypeUnitOfMeasurement </font>
                </Col>
                <Col span={12}>
                  <Input
                    type="text"
                    value={String(
                      selectedNomenclatureItem.typeUnitOfMeasurement
                    )}
                  />
                </Col>
              </Row>
              <br />
              <Row>
                <Col span={12}>
                  <font size="3"> ValueUnitOfMeasurement </font>
                </Col>
                <Col span={12}>
                  <Input
                    type="text"
                    value={String(
                      selectedNomenclatureItem.valueUnitOfMeasurement
                    )}
                  />
                </Col>
              </Row>
              <br />
              <Row>
                <Col span={12}>
                  <font size="3"> MeasurementDescription </font>
                </Col>
                <Col span={12}>
                  <Input
                    type="text"
                    value={String(
                      selectedNomenclatureItem.measurementDescription
                    )}
                  />
                </Col>
              </Row>
              <br />
              <Row>
                <Col span={12}>
                  <font size="3"> ShowComposition </font>
                </Col>
                <Col span={12}>
                  <Switch
                    checkedChildren={<CheckOutlined />}
                    unCheckedChildren={<CloseOutlined />}
                    defaultChecked={selectedNomenclatureItem.showComposition}
                    size="default"
                    disabled={true}
                  />
                </Col>
              </Row>
              <br />
              <Row>
                <Col span={12}>
                  <font size="3"> Barcode </font>
                </Col>
                <Col span={12}>
                  <Input
                    type="text"
                    value={String(selectedNomenclatureItem.barcode)}
                  />
                </Col>
              </Row>
              <br />
              <Row>
                <Col span={12}>
                  <font size="3"> imagesJSON </font>
                </Col>
                <Col span={12}>
                  <Input
                    type="text"
                    value={String(selectedNomenclatureItem.imagesJSON)}
                  />
                </Col>
              </Row>
              <br />
              <Row>
                <Col span={12}>
                  <font size="3"> CommerseDescription </font>
                </Col>
                <Col span={12}>
                  <Input
                    type="text"
                    value={String(selectedNomenclatureItem.commerseDescription)}
                  />
                </Col>
              </Row>
              <br />
              <Row>
                <Col span={12}>
                  <font size="3"> NameFeature </font>
                </Col>
                <Col span={12}>
                  <Input
                    type="text"
                    value={String(selectedNomenclatureItem.nameFeature)}
                  />
                </Col>
              </Row>
              <br />
              <Row>
                <Col span={12}>
                  <font size="3"> AdditionalInfo </font>
                </Col>
                <Col span={12}>
                  <Input
                    type="text"
                    value={String(selectedNomenclatureItem.additionaInfo)}
                  />
                </Col>
              </Row>
              <br />
              <Row>
                <Col span={12}>
                  <font size="3"> AdditionaInfoJSON </font>
                </Col>
                <Col span={12}>
                  <Input
                    type="text"
                    value={String(selectedNomenclatureItem.additionaInfoJSON)}
                  />
                </Col>
              </Row>
              {/* ------------ данные товара ----------------- */}
              <br />
              <font size="3">
                <b> Данные товара </b>
              </font>
              <Divider />
              <Row>
                <Col span={12}>
                  <font size="3"> ExtendedDescription </font>
                </Col>
                <Col span={12}>
                  <Input
                    onChange={(e) => setExtendedDescription(e.target.value)}
                    type="text"
                  />
                </Col>
              </Row>
              <br />
              <Row>
                <Col span={12}>
                  <font size="3"> URL </font>
                </Col>
                <Col span={12}>
                  <Input onChange={(e) => setUrl(e.target.value)} type="url" />
                </Col>
              </Row>
              <br />
              <Row>
                <Col span={12}>
                  <font size="3"> PopularOrder </font>
                </Col>
                <Col span={12}>
                  <Input
                    onChange={(e) => setPopularOrder(Number(e.target.value))}
                    type="number"
                  />
                </Col>
              </Row>
              <br />
              <Row>
                <Col span={12}>
                  <font size="3"> Rating </font>
                </Col>
                <Col span={12}>
                  <Input
                    onChange={(e) => setRating(Number(e.target.value))}
                    type="number"
                  />
                </Col>
              </Row>
              <br />
              <Row>
                <Col span={12}>
                  <font size="3"> DefectName </font>
                </Col>
                <Col span={12}>
                  <Input
                    onChange={(e) => setDefectName(e.target.value)}
                    type="text"
                  />
                </Col>
              </Row>
              <br />
              <Row>
                <Col span={12}>
                  <font size="3"> Kiz </font>
                </Col>
                <Col span={12}>
                  <Input onChange={(e) => setKiz(e.target.value)} type="text" />
                </Col>
              </Row>
              <br />
              <Row>
                <Col span={12}>
                  <font size="3"> Quantity </font>
                </Col>
                <Col span={12}>
                  <Input
                    onChange={(e) => setQuantity(Number(e.target.value))}
                    type="number"
                    defaultValue={0}
                  />
                </Col>
              </Row>
              <br />
              <Row>
                <Col span={12}>
                  <font size="3"> BeginPrice </font>
                </Col>
                <Col span={12}>
                  <Input
                    onChange={(e) => setBeginPrice(Number(e.target.value))}
                    type="number"
                    defaultValue={0}
                  />
                </Col>
              </Row>
              <br />
              <Row>
                <Col span={12}>
                  <font size="3"> CurrentPrice </font>
                </Col>
                <Col span={12}>
                  <Input
                    onChange={(e) => setCurrentPrice(Number(e.target.value))}
                    type="number"
                    defaultValue={0}
                  />
                </Col>
              </Row>
              <br />
              <Row>
                <Col span={12}>
                  <font size="3"> CostPrice </font>
                </Col>
                <Col span={12}>
                  <Input
                    onChange={(e) => setCostPrice(Number(e.target.value))}
                    type="number"
                    defaultValue={0}
                  />
                </Col>
              </Row>
              <br />
              <Row>
                <Col span={12}>
                  <font size="3"> CosttPriceBasicUnit </font>
                </Col>
                <Col span={12}>
                  <Input
                    onChange={(e) =>
                      setCosttPriceBasicUnit(Number(e.target.value))
                    }
                    type="number"
                    defaultValue={0}
                  />
                </Col>
              </Row>
              <br />
              <Row>
                <Col span={12}>
                  <font size="3"> MaxValueDiscount </font>
                </Col>
                <Col span={12}>
                  <Input
                    onChange={(e) =>
                      setMaxValueDiscount(Number(e.target.value))
                    }
                    type="number"
                    defaultValue={0}
                  />
                </Col>
              </Row>
              <br />
              <Button size="large" type="primary" onClick={submitHandler}>
                Сохранить
              </Button>
            </>
          )}
        </Card>
      )}
    </Card>
  );
};

export default ShopProducts;
