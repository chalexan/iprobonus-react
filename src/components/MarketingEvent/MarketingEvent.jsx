import "./MarketingEvent.css";
import TimePicker from "rc-time-picker";
import "rc-time-picker/assets/index.css";
import moment from "moment";
import { Card } from "antd";
import { Breadcrumb } from "antd";
import { HomeOutlined, UploadOutlined } from "@ant-design/icons";
import {
  Form,
  Input,
  Button,
  Space,
  DatePicker,
  Select,
  Checkbox,
  Badge,
  Upload,
  message,
  Alert,
} from "antd";
import { useState } from "react";

const MarketingEvent = () => {
  const { RangePicker } = DatePicker;
  const { Option } = Select;
  const winWidth = window.innerWidth;

  const [allClientsState, setAllClientsState] = useState(false);
  const [smsState, setSmsState] = useState(false);
  const [callState, setCallState] = useState(false);
  const [pushState, setPushState] = useState(false);
  const [inAppState, setInAppState] = useState(false);
  const [isTextState, setTextState] = useState(true);
  const [bonusState, setBonusState] = useState(false);
  const [emailState, setEmailState] = useState(false);

  // Заполнение списков - переделать на корректные значения

  const children = [];
  for (let i = 10; i < 36; i++) {
    children.push(
      <Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>
    );
  }

  // Массив ошибок ввода

  const [errArray, setErrArray] = useState([]);
  let isInputError = false;

  // Toggle states in UI

  const allClientsHandler = (e) => {
    setAllClientsState(e.target.checked);
  };

  const smsHandler = (e) => {
    setSmsState(e.target.checked);
  };

  const callHandler = (e) => {
    setCallState(e.target.checked);
  };

  const pushHandler = (e) => {
    setPushState(e.target.checked);
  };

  const inAppHandler = (e) => {
    setInAppState(e.target.checked);
  };

  const textPromoHandler = (e) => {
    e === "text" ? setTextState(true) : setTextState(false);
  };

  const bonusHandler = (e) => {
    setBonusState(e.target.checked);
  };

  const emailHandler = (e) => {
    setEmailState(e.target.checked);
  };

  // Хранение текущих значений полей пользователя

  const [eventName, setEventName] = useState(null);
  const [eventDescription, setEventDescription] = useState(null);

  // Обработчик кнопки "сохранить как черновик"

  const saveHandler = () => {
    isInputError = false;
    setErrArray([]);
    // Функция проверки на валидность данных из полей пользователя

    const checkFields = () => {
      if (!eventName) {
        setErrArray((prevState) => [
          ...prevState,
          "Добавьте название мероприятия",
        ]);
        isInputError = true;
      }

      if (!eventDescription) {
        setErrArray((prevState) => [
          ...prevState,
          "Добавьте описание мероприятия",
        ]);
        isInputError = true;
      }
      return !isInputError;
    };

    if (!checkFields()) message.error("Ошибка заполнения формы");
  };

  return (
    <div className="site-card-border-less-wrapper">
      <h4>Создать новое мероприятие</h4>
      <Breadcrumb>
        <Breadcrumb.Item href="">
          <HomeOutlined />
        </Breadcrumb.Item>
        <Breadcrumb.Item href="">
          <span>Маркетинг</span>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Создать новое мероприятие</Breadcrumb.Item>
      </Breadcrumb>
      <br />
      <Card
        title="Заполните основную информацию"
        bordered={false}
        style={{ width: { winWidth } }}
      >
        {errArray.length !== 0
          ? errArray.map((error) => {
              return (
                <div>
                  <Alert message={error} type="error" />
                  <br />
                </div>
              );
            })
          : null}

        <Form layout="vertical">
          <Form.Item label="Название мероприятия">
            <Input
              placeholder=""
              onChange={(e) => {
                setEventName(e.target.value);
              }}
            />
          </Form.Item>
          <Form.Item label="Описание мероприятия">
            <Input.TextArea
              onChange={(e) => {
                setEventDescription(e.target.value);
              }}
            />
          </Form.Item>
        </Form>
        <p>Дата и время начала и завершения мероприятия</p>
        <RangePicker defaultValue={[moment(), moment()]} />
        <br />
        <br />
        <p>Время начала мероприятия: </p>
        <TimePicker defaultValue={moment()} />
        <br />
        <br />
        <p>Время окончания мероприятия: </p>
        <TimePicker defaultValue={moment()} />
      </Card>
      <Card
        title="Тестовый блок"
        bordered={false}
        style={{ width: { winWidth } }}
      >
        <Form layout="vertical">
          <Form.Item label="Выберите тестовый сегмент для мероприятия">
            <p>
              *Чтобы запускать тесты необходимо записать маркетинговое
              мероприятие
            </p>
            <Select defaultValue="lucy" style={{ width: 200 }}>
              <Option value="jack">Jack</Option>
              <Option value="Yiminghe">yiminghe</Option>
            </Select>
            <span> </span>
            <Button type="primary"> Провести тест </Button>
          </Form.Item>
          <Form.Item label="Выберите сегменты фильтра">
            <p>
              <Checkbox onChange={allClientsHandler}>Все клиенты</Checkbox>
            </p>
            {!allClientsState ? (
              <Select
                mode="tags"
                placeholder="Please select"
                style={{ width: "100%" }}
              >
                {children}
              </Select>
            ) : null}
          </Form.Item>
        </Form>
      </Card>
      <Card
        title="Добавьте каналы рассылки"
        bordered={false}
        style={{ width: { winWidth } }}
      >
        <p>Выбор каналов рассылки:</p>
        <Card>
          <p>
            <Checkbox onChange={smsHandler}>Добавить рассылку SMS</Checkbox>
          </p>
          {smsState ? (
            <Form layout="vertical">
              <Form.Item label="Дата рассылки SMS">
                <DatePicker />
              </Form.Item>
              <Form.Item label="Текст сообщения">
                <Input.TextArea />
              </Form.Item>
              <Form.Item label="Укажите сумму SMS рассылки">
                <Input prefix="₽" suffix="RUB" />
              </Form.Item>
            </Form>
          ) : null}
        </Card>
        <br />
        <Card>
          <p>
            <Checkbox onChange={callHandler}>Добавить обзвон</Checkbox>
          </p>
          {callState ? (
            <Form layout="vertical">
              <Form.Item label="Дата обзвона">
                <DatePicker />
              </Form.Item>
              <Form.Item label="Текст обзвона">
                <Input.TextArea />
              </Form.Item>
              <Form.Item label="Укажите сумму обзвона">
                <Input prefix="₽" suffix="RUB" />
              </Form.Item>
            </Form>
          ) : null}
        </Card>
        <br />
        <Card>
          <p>
            <Checkbox onChange={pushHandler}>
              Добавить рассылку Push уведомлений
            </Checkbox>
          </p>
          <Badge.Ribbon text="Бесплатная рассылка"></Badge.Ribbon>
          {pushState ? (
            <Form layout="vertical">
              <Form.Item label="Дата рассылки push-уведомления">
                <DatePicker />
              </Form.Item>
              <Form.Item label="Текст уведомления">
                <Input.TextArea />
              </Form.Item>
            </Form>
          ) : null}
        </Card>
        <br />
        <Card>
          <p>
            <Checkbox onChange={inAppHandler}>Добавить рассылку inApp</Checkbox>
          </p>
          <Badge.Ribbon text="Бесплатная рассылка"></Badge.Ribbon>
          {inAppState ? (
            <Form layout="vertical">
              <Form.Item label="Тип сообщения">
                <Select
                  defaultValue="text"
                  style={{ width: 200 }}
                  onChange={textPromoHandler}
                >
                  <Option value="text">Текстовое</Option>
                  <Option value="promo">Промо</Option>
                </Select>
              </Form.Item>
              <Form.Item label="Заголовок">
                <Input placeholder="" />
              </Form.Item>
              <Form.Item label="Сообщение">
                <Input.TextArea />
              </Form.Item>
              {!isTextState ? (
                <>
                  <Form.Item label="Текст подвала">
                    <Input.TextArea />
                  </Form.Item>
                </>
              ) : null}
              <Form.Item label="Дата актуальности">
                <DatePicker showTime />
              </Form.Item>
              <Form.Item label="Дата отправки">
                <DatePicker showTime />
              </Form.Item>
              {!isTextState ? (
                <>
                  <Form.Item label="Ссылка на изображение">
                    <Input placeholder="" />
                  </Form.Item>
                  <Form.Item label="Файл">
                    <Upload
                      onChange={(e) => {
                        console.log(e.file.name);
                      }}
                    >
                      <Button icon={<UploadOutlined />}>Click to Upload</Button>
                    </Upload>
                  </Form.Item>
                </>
              ) : null}
            </Form>
          ) : null}
        </Card>
        <br />
        <Card>
          <p>
            <Checkbox onChange={bonusHandler}>
              Добавить начисление бонусов
            </Checkbox>
          </p>
          {bonusState ? (
            <Form layout="vertical">
              <>
                <Form.Item label="Cумма начисляемых бонусов">
                  <Input placeholder="" />
                </Form.Item>
              </>
            </Form>
          ) : null}
        </Card>
        <br />
        <Card>
          <p>
            <Checkbox onChange={emailHandler}>Добавить рассылку email</Checkbox>
          </p>
          {emailState ? (
            <Form layout="vertical">
              <>
                <Form.Item label="Дата рассылки E-mail">
                  <DatePicker showTime />
                </Form.Item>
                <Form.Item label="Тип рассылки E-mail">
                  <Select defaultValue="personal" style={{ width: 200 }}>
                    <Option value="personal">Персональное письмо</Option>
                    <Option value="template">Шаблон</Option>
                  </Select>
                </Form.Item>
                <Form.Item label="Выберите шаблон рассылки E-mail">
                  <Select defaultValue="template1" style={{ width: 200 }}>
                    <Option value="template1">Без шаблона</Option>
                    <Option value="template2">Шаблон</Option>
                  </Select>
                </Form.Item>
                <Form.Item label="Текст письма">
                  <Input.TextArea />
                </Form.Item>
                <Form.Item label="Укажите сумму E-mail рассылки">
                  <Input prefix="₽" suffix="RUB" />
                </Form.Item>
              </>
            </Form>
          ) : null}
        </Card>
      </Card>
      <div className="button-footer">
        <Space size="large">
          <Button type="primary" onClick={saveHandler}>
            {" "}
            Сохранить как черновик{" "}
          </Button>
          <Button type="primary"> Подтвердить мероприятие </Button>
        </Space>
      </div>
    </div>
  );
};

export default MarketingEvent;
