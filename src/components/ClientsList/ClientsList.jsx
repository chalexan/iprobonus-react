import "./ClientsList.css";
import { Card, Table } from "antd";
import { Breadcrumb } from "antd";
import { HomeOutlined, SearchOutlined } from "@ant-design/icons";
import { Input, Button, Space } from "antd";
import { useState } from "react";
import { useHistory } from "react-router-dom";
import Highlighter from "react-highlight-words";
//import data from "../MockUsers/mockUsers";
import { useSelector } from "react-redux";

const ClientsList = () => {
  const winWidth = window.innerWidth;
  const { Search } = Input;
  const histoty = useHistory();
  const { data } = useSelector((state) => state);

  const newData = data.map((user) => {
    return {
      ...user,
      fio: user.firstname + " " + user.surname + " " + user.fathername,
    };
  });

  //Поиск по всем колонкам

  const [dataSource, setDataSource] = useState(newData);
  const filterAllData = (e) => {
    const filteredData = newData.filter(
      (entry) =>
        entry.fio.includes(e) ||
        entry.phone.includes(e) ||
        entry.id.includes(e) ||
        entry.email.includes(e)
    );
    setDataSource(filteredData);
  };

  // Поиск по колонке

  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  let searchInput;

  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node) => {
            searchInput = node;
          }}
          placeholder={`Введите запрос`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Поиск
          </Button>
          <Button
            onClick={() => handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Сброс
          </Button>
          <Button
            type="link"
            size="small"
            onClick={() => {
              confirm({ closeDropdown: false });
              setSearchText(selectedKeys[0]);
              setSearchedColumn(dataIndex);
            }}
          ></Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex]
            .toString()
            .toLowerCase()
            .includes(value.toLowerCase())
        : "",
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.select(), 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      ),
  });

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();

    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText("");
  };

  // Конец поиска по колонке

  // Обработчик перехода на подробности

  const handleDetails = (id) => {
    histoty.push(`/Clients/Info/${id}`);
  };

  // Описание столбцов

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      ...getColumnSearchProps("id"),
    },
    {
      title: "ФИО",
      dataIndex: "fio",
      ...getColumnSearchProps("fio"),
      sorter: {
        compare: (a, b) => a.key - b.key,
      },
    },
    {
      title: "Телефон",
      dataIndex: "phone",
      ...getColumnSearchProps("phone"),
      sorter: {
        compare: (a, b) => a.key - b.key,
      },
    },
    {
      title: "Email",
      dataIndex: "email",
      ...getColumnSearchProps("email"),
      sorter: {
        compare: (a, b) => a.key - b.key,
      },
    },
    {
      title: "Действие",
      dataIndex: "",
      key: "x",
      render: (_, record) => (
        <a onClick={() => handleDetails(record.id)}>Подробнее</a>
      ),
    },
  ];

  return (
    <div className="site-card-border-less-wrapper">
      <h4>Список клиентов</h4>
      <Breadcrumb>
        <Breadcrumb.Item href="">
          <HomeOutlined />
        </Breadcrumb.Item>
        <Breadcrumb.Item href="">
          <span>Клиенты</span>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Список клиентов</Breadcrumb.Item>
      </Breadcrumb>
      <br />
      <Card
        title="Список клиентов"
        bordered={false}
        style={{ width: { winWidth } }}
      >
        <div className="left-right">
          <div></div>
          <div>
            <Search
              placeholder="Поиск..."
              size="large"
              onSearch={filterAllData}
              enterButton
            />
          </div>
        </div>
        <br />
        <Table
          columns={columns}
          dataSource={dataSource}
          pagination={{ position: ["none", "bottomCenter"] }}
        />
      </Card>
    </div>
  );
};

export default ClientsList;
