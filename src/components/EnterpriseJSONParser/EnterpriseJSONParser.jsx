import { useSelector, useDispatch } from "react-redux";
import { useEffect, useState } from "react";
import { message, Card } from "antd";
import "./EnterpriseJSONParser.css";
import * as constant from "../constant";

const EnterpriseJSONParser = () => {
  const winWidth = window.innerWidth;
  const { currentEnterpriseId } = useSelector((state) => state);
  const [enterpriseJSON, setEnterpriseJSON] = useState("");
  const [
    iDataInternalEmailIntegrationsData,
    setIDataInternalEmailIntegrationsData,
  ] = useState("");
  const [iDataSMSData, setIDataSMSData] = useState("");
  const [iDataPushDataAndroid, setIDataPushDataAndroid] = useState("");
  const [iDataPushDataIOS, setIDataPushDataIOS] = useState("");
  const [iDataTransactionalEmailOptions, setIDataTransactionalEmailOptions] =
    useState("");
  const [iDataUnisenderOptions, setIDataUnisenderOptions] = useState("");
  const dispatch = useDispatch();

  useEffect(() => {
    getEnterpriseJSON();
  }, [currentEnterpriseId]);

  // Функция вытягивает JSON из организации
  const getEnterpriseJSON = async () => {
    const token = localStorage.getItem("token");
    const api = constant.API;
    const response = await fetch(`${api}/api/enterprise/`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `bearer ${token}`,
        AccessKey: constant.ACCESSKEY,
      },
    });
    if (response.status === 401 || response.status === 403) {
      return dispatch({ type: "LOGOUT" });
    }
    const result = await response.json();

    // проверка на корректность ответа
    if (result.result.codeResult === 0) {
      const enterprises = result.dataList;
      const enterpriseInformation = enterprises.filter(
        (el) => el.idUnique === currentEnterpriseId
      );
      //   if (enterpriseStringJSON)
      //     setEnterpriseJSON(JSON.parse(enterpriseStringJSON));
      if (enterpriseInformation[0] === undefined) return;
      if (enterpriseInformation[0] === null) return;
      console.log(JSON.parse(enterpriseInformation[0].dataJSON));
      const temporary = JSON.parse(enterpriseInformation[0].dataJSON);

      setEnterpriseJSON(JSON.parse(enterpriseInformation[0].dataJSON));
      if (temporary === undefined) return;
      if (temporary === null) return;
      if (temporary.integrationsListData === null) return;
      setIDataInternalEmailIntegrationsData(
        JSON.parse(temporary.integrationsListData.InternalEmailIntegrationsData)
      );
      setIDataSMSData(JSON.parse(temporary.integrationsListData.SMSData));
      setIDataPushDataAndroid(
        JSON.parse(temporary.integrationsListData.PushDataAndroid)
      );
      setIDataPushDataIOS(
        JSON.parse(temporary.integrationsListData.PushDataIOS)
      );
      setIDataTransactionalEmailOptions(
        JSON.parse(temporary.integrationsListData.TransactionalEmailOptions)
      );
      setIDataUnisenderOptions(
        JSON.parse(temporary.integrationsListData.UnisenderOptions)
      );
    } else message.error(result.result.message);
  };

  return (
    <>
      <Card title="JSONParser" bordered={true} style={{ width: { winWidth } }}>
        {enterpriseJSON && (
          <div>
            <Card
              title="Общая информация"
              bordered={false}
              style={{ width: { winWidth } }}
            >
              <b>IDBonusTypeForNegativeBalance:</b>
              <br />
              {enterpriseJSON.IDBonusTypeForNegativeBalance}
              <br />
              <br />
              <b>EmailsForSendTestimonials:</b>
              <br />
              {enterpriseJSON.EmailsForSendTestimonials}
              <br />
              <br />
              <b>TypeName:</b>
              <br />
              {enterpriseJSON.TypeName}
            </Card>
            {enterpriseJSON.DefaultShops !== null ? (
              <>
                <Card
                  title="DefaultShops"
                  bordered={false}
                  style={{ width: { winWidth } }}
                >
                  <b>IDAppShop:</b>
                  <br />
                  {enterpriseJSON.DefaultShops.IDAppShop}
                  <br />
                  <br />
                  <b>IDWebShop:</b>
                  <br />
                  {enterpriseJSON.DefaultShops.IDWebShop}
                </Card>
              </>
            ) : null}
            {enterpriseJSON.RuleBirthday !== null ? (
              <>
                <Card
                  title="RuleBirthday"
                  bordered={false}
                  style={{ width: { winWidth } }}
                >
                  <b>IDRFTypeBonuses:</b>
                  <br />
                  {enterpriseJSON.RuleBirthday.IDRFTypeBonuses}
                  <br />
                  <br />
                  <b>SumBonuses:</b>
                  <br />
                  {enterpriseJSON.RuleBirthday.SumBonuses}
                  <br />
                  <br />
                  <b>TypeName:</b>
                  <br />
                  {String(enterpriseJSON.RuleBirthday.TypeName)}
                </Card>
              </>
            ) : null}
            {enterpriseJSON.RuleSale !== null ? (
              <>
                <Card
                  title="RuleSale"
                  bordered={false}
                  style={{ width: { winWidth } }}
                >
                  <b>IDRFTypeBonuses:</b>
                  <br />
                  {enterpriseJSON.RuleSale.IDRFTypeBonuses}
                  <br />
                  <br />
                  <b>SumBonuses:</b>
                  <br />
                  {enterpriseJSON.RuleSale.SumBonuses}
                  <br />
                  <br />
                  <b>TypeName:</b>
                  <br />
                  {String(enterpriseJSON.RuleSale.TypeName)}
                </Card>
              </>
            ) : null}
            {enterpriseJSON.RuleWelcome !== null ? (
              <>
                <Card
                  title="RuleWelcome"
                  bordered={false}
                  style={{ width: { winWidth } }}
                >
                  <b>IDRFTypeBonuses:</b>
                  <br />
                  {enterpriseJSON.RuleWelcome.IDRFTypeBonuses}
                  <br />
                  <br />
                  <b>SumBonuses:</b>
                  <br />
                  {enterpriseJSON.RuleWelcome.SumBonuses}
                  <br />
                  <br />
                  <b>TypeName:</b>
                  <br />
                  {String(enterpriseJSON.RuleWelcome.TypeName)}
                </Card>
              </>
            ) : null}
            {enterpriseJSON.integrationsListData !== null ? (
              <>
                <Card
                  title="integrationsListData"
                  bordered={false}
                  style={{ width: { winWidth } }}
                >
                  <Card
                    title="InternalEmailIntegrationsData"
                    bordered={false}
                    style={{ width: { winWidth } }}
                  >
                    {console.log(
                      "InternalEmailIntegrationsData",
                      iDataInternalEmailIntegrationsData
                    )}
                    <b>TypeName:</b>
                    <br />
                    {iDataInternalEmailIntegrationsData.TypeName}
                    <br />
                    <br />
                    <b>emailFrom:</b>
                    <br />
                    {String(iDataInternalEmailIntegrationsData.emailFrom)}
                    <br />
                    <br />
                    <b>pawwsord:</b>
                    <br />
                    {String(iDataInternalEmailIntegrationsData.pawwsord)}
                    <br />
                    <br />
                    <b>serverOutcoming:</b>
                    <br />
                    {String(iDataInternalEmailIntegrationsData.serverOutcoming)}
                    <br />
                    <br />
                    <b>userName:</b>
                    <br />
                    {String(iDataInternalEmailIntegrationsData.userName)}
                  </Card>
                  <Card
                    title="SMSData"
                    bordered={false}
                    style={{ width: { winWidth } }}
                  >
                    {console.log("SMSData", iDataSMSData)}
                    <b>Gate:</b>
                    <br />
                    {String(iDataSMSData.Gate)}
                    <br />
                    <br />
                    <b>Login:</b>
                    <br />
                    {String(iDataSMSData.Login)}
                    <br />
                    <br />
                    <b>Password:</b>
                    <br />
                    {String(iDataSMSData.Password)}
                    <br />
                    <br />
                    <b>SenderName:</b>
                    <br />
                    {String(iDataSMSData.SenderName)}
                    <br />
                    <br />
                    <b>TypeName:</b>
                    <br />
                    {String(iDataSMSData.TypeName)}
                  </Card>
                  <Card
                    title="PushDataAndroid"
                    bordered={false}
                    style={{ width: { winWidth } }}
                  >
                    {console.log("PushDataAndroid", iDataPushDataAndroid)}
                    <b>AuthorizationKey:</b>
                    <br />
                    {String(iDataPushDataAndroid.AuthorizationKey)}
                    <br />
                    <br />
                    <b>TypeName:</b>
                    <br />
                    {String(iDataPushDataAndroid.TypeName)}
                  </Card>
                  <Card
                    title="PushDataIOS"
                    bordered={false}
                    style={{ width: { winWidth } }}
                  >
                    {console.log("PushDataIOS", iDataPushDataIOS)}
                    <b>FileNameSertIO:</b>
                    <br />
                    {String(iDataPushDataIOS.FileNameSertIO)}
                    <br />
                    <br />
                    <b>PasswodrToSertIOS:</b>
                    <br />
                    {String(iDataPushDataIOS.PasswodrToSertIOS)}
                    <br />
                    <br />
                    <b>TypeName:</b>
                    <br />
                    {String(iDataPushDataIOS.TypeName)}
                  </Card>
                  <Card
                    title="TransactionalEmailOptions"
                    bordered={false}
                    style={{ width: { winWidth } }}
                  >
                    {console.log(
                      "TransactionalEmailOptions",
                      iDataTransactionalEmailOptions
                    )}
                    <b>SenderEmail:</b>
                    <br />
                    {String(iDataTransactionalEmailOptions.SenderEmail)}
                    <br />
                    <br />
                    <b>SenderName:</b>
                    <br />
                    {String(iDataTransactionalEmailOptions.SenderName)}
                    <br />
                    <br />
                    <b>UniOneApiKey:</b>
                    <br />
                    {String(iDataTransactionalEmailOptions.UniOneApiKey)}
                    <br />
                    <br />
                    <b>UniOneLogin:</b>
                    <br />
                    {String(iDataTransactionalEmailOptions.UniOneLogin)}
                    <br />
                    <br />
                    <b>TypeName:</b>
                    <br />
                    {String(iDataTransactionalEmailOptions.TypeName)}
                  </Card>
                </Card>
              </>
            ) : null}
          </div>
        )}
      </Card>
    </>
  );
};

export default EnterpriseJSONParser;
