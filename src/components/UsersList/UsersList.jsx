import "./UsersList.css";
import * as constant from "../constant";
import { Card } from "antd";
import { Breadcrumb } from "antd";
import { HomeOutlined, SearchOutlined } from "@ant-design/icons";
import { Input, Button, Space, Table } from "antd";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import Highlighter from "react-highlight-words";

const UsersList = (props) => {
  const winWidth = window.innerWidth;
  const dispatch = useDispatch();
  const { Search } = Input;
  const { currentEnterpriseId } = useSelector((state) => state);
  const { changes } = useSelector((state) => state);
  const { currentEnterpriseName } = useSelector((state) => state);
  const [user, setUser] = useState("");

  useEffect(() => {
    getUsers();
  }, [currentEnterpriseId]);
  // запрос на получение списка пользователей организации

  const getUsers = async () => {
    const token = localStorage.getItem("token");
    const api = constant.API;
    const response = await fetch(
      `${api}/api/enterprise/user/list?IDEnterprise=${currentEnterpriseId}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `bearer ${token}`,
          AccessKey: constant.ACCESSKEY,
        },
      }
    );

    if (response.status === 401) {
      return dispatch({ type: "LOGOUT" });
    }
    if (!response.ok)
      return dispatch({
        type: "SET_DEBUG_MESSAGE",
        payload: response.statusText,
      });
    const result = await response.json();

    // проверка на корректность ответа
    if (result.result.status === 0) {
      dispatch({
        type: "SET_DEBUG_MESSAGE",
        payload: response.statusText,
      });
      setUser(result.dataList);
    } else alert("server error");
  };

  // Поиск по колонке

  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  let searchInput;

  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node) => {
            searchInput = node;
          }}
          placeholder={`Введите запрос`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Поиск
          </Button>
          <Button
            onClick={() => handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Сброс
          </Button>
          <Button
            type="link"
            size="small"
            onClick={() => {
              confirm({ closeDropdown: false });
              setSearchText(selectedKeys[0]);
              setSearchedColumn(dataIndex);
            }}
          ></Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex]
            .toString()
            .toLowerCase()
            .includes(value.toLowerCase())
        : "",
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.select(), 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      ),
  });

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();

    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText("");
  };

  // Конец поиска по колонке

  // Обработчик перехода на подробности

  const handleDetails = (id) => {
    alert("modal");
  };

  // Описание столбцов

  const columns = [
    {
      title: "ID",
      dataIndex: "idUnique",
      ...getColumnSearchProps("idUnique"),
      sorter: {
        compare: (a, b) => a.key - b.key,
      },
    },
    {
      title: "Имя",
      dataIndex: "name",
      ...getColumnSearchProps("name"),
      sorter: {
        compare: (a, b) => a.key - b.key,
      },
    },
    {
      title: "E-mail",
      dataIndex: "email",
      ...getColumnSearchProps("email"),
      sorter: {
        compare: (a, b) => a.key - b.key,
      },
    },
    {
      title: "Действие",
      dataIndex: "",
      key: "x",
      render: (_, record) => (
        <a onClick={() => handleDetails(record.idUnique)}>Сменить пароль</a>
      ),
    },
  ];
  return (
    <Table
      columns={columns}
      // Пофиксить dataSource - все же попробывть вытянуть из useState
      dataSource={user}
      pagination={{ position: ["none", "bottomCenter"] }}
    />
  );
};

export default UsersList;
