import { useDispatch, useSelector } from "react-redux";
import { Menu, Dropdown } from "antd";
import { DownOutlined } from "@ant-design/icons";
import DebugInfo from "../DebugInfo/DebugInfo";

const HeaderInfo = () => {
  const dispatch = useDispatch();
  const { username } = useSelector((state) => state);

  return (
    <>
      <div className="left-right">
        <div style={{ paddingLeft: "20px" }}>
          <img src="/assets/img/person.png" alt="logo" width="30px" />
          <b> {username}</b>
        </div>
        <a
          style={{ paddingRight: "20px" }}
          onClick={() => {
            dispatch({ type: "LOGOUT" });
          }}
        >
          Logout
        </a>
      </div>
    </>
  );
};

export default HeaderInfo;
