import "./EnterpriseCollaborateInfo.css";
import * as constant from "../constant";
import { Card, message, Row, Col, Divider, Modal } from "antd";
import { Input, Button, Image, Switch, Select } from "antd";
import { CopyOutlined, CheckOutlined, CloseOutlined } from "@ant-design/icons";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import Uploader from "../Uploader/Uploader";

const EnterpriseCollaborateInfo = (props) => {
  const winWidth = window.innerWidth;
  const dispatch = useDispatch();
  const { currentEnterpriseName } = useSelector((state) => state);
  const { uploadType } = useSelector((state) => state);
  const { uploadUrlRedux } = useSelector((state) => state);
  const [spinner, setSpinner] = useState(false);
  const [collaborateData, setcollaborateData] = useState();
  const [offerData, setOfferData] = useState();
  const [modalFile, setModalFile] = useState(false);

  // Данные этой формы - они приходят, изменяются и уходят:
  // Приходят - с АПИ и сразу заносим в соответствующие стейты
  // Изменяются - обработчики событий OnChange меняют стейт
  // Уходят - когда после валидации данныых в этих стейтах мы их высылаем обратно методом POST

  const [headImageURL, setHeadImageURL] = useState("");
  const [logoImageURL, setLogoImageURL] = useState("");
  const [miniImageURL, setMiniImageURL] = useState("");

  const [title, setTitle] = useState("");
  const [subTitle, setSubTitle] = useState("");
  const [fullDescription, setFullDescription] = useState("");
  const [headDescription, setHeadDescription] = useState("");
  const [websiteURL, setWebsiteURL] = useState("");
  const [acceptBonuses, setAcceptBonuses] = useState(0);
  const [enrollBonuses, setEnrollBonuses] = useState(0);
  const [delivery, setDelivery] = useState(false);

  // Это переменная в которой лежат города полученные из API
  const [cities, setCities] = useState("");

  // Здесь хранятcя id данные выбранных городов
  const [selCities, setSelCities] = useState("");

  // Здесь хранятся загруженные города организации
  const [loadedCities, setLoadedCities] = useState(null);

  const { Option } = Select;

  useEffect(() => {
    console.log(uploadType);
    getCities();
    if (uploadType === "HeadImageURL") {
      setHeadImageURL(uploadUrlRedux);
    }
    if (uploadType === "LogoImageURL") {
      setLogoImageURL(uploadUrlRedux);
    }
    if (uploadType === "MiniImageURL") {
      setMiniImageURL(uploadUrlRedux);
    }
  }, [uploadUrlRedux]);

  // Формаирование строки из id выбранных городов из строки названий

  const findCityIdbyName = (value) => {
    const tempArray = [];
    for (let i = 0; i < value.length; i += 1) {
      tempArray.push(cities.find((el) => el.name === value[i]));
    }
    tempArray.map((el) => el.idUnique);
    console.log(
      "modify",
      tempArray.map((el) => el.idUnique)
    );
    setSelCities(tempArray.map((el) => el.idUnique).join(","));
  };

  // Получение списка всех доступных городов

  const getCities = async () => {
    const token = localStorage.getItem("token");
    const api = constant.APISHOP;

    const response = await fetch(
      `${api}/iecommercecore/api/v1/shop/city/list`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `bearer ${token}`,
          AccessKey: constant.ACCESSKEY,
        },
      }
    );
    if (response.status === 401) {
      return dispatch({ type: "LOGOUT" });
    }
    if (response.status !== 200) return;
    const result = await response.json();
    console.log("cities", result.dataList);
    await setCities(result.dataList);
  };

  //  Получение данных организации - collaborationAPI

  const getCollaborationData = async () => {
    //  Запрос на получение данных

    setSpinner(true);

    const response = await fetch(
      `${constant.APIСOLLABORATIONIPB}/web/enterprise/list/${props.uuid}`
    );

    const responseCollaboration = await fetch(
      `${constant.APIСOLLABORATIONIPB}/collaboration/offers/web/list/${props.uuid}`
    );

    setSpinner(false);

    if (!response.ok)
      return dispatch({
        type: "SET_DEBUG_MESSAGE",
        payload: response.statusText,
      });

    if (!responseCollaboration.ok)
      return dispatch({
        type: "SET_DEBUG_MESSAGE",
        payload: responseCollaboration.statusText,
      });

    const result = await response.json();
    const resultCollaboration = await responseCollaboration.json();
    console.log("Result collaboration", resultCollaboration.dataList.length);

    // Проверить dataList - если он пустой, то создать новые данные об организации

    if (result.dataList.length > 0) {
      setcollaborateData(result.dataList[0]);

      setTitle(result.dataList[0].title);
      setSubTitle(result.dataList[0].subTitle);
      setWebsiteURL(result.dataList[0].websiteURL);
      setFullDescription(result.dataList[0].fullDescription);
      setEnrollBonuses(result.dataList[0].enrollBonuses);
      setAcceptBonuses(result.dataList[0].acceptBonuses);
      setDelivery(result.dataList[0].delivery);
      setHeadImageURL(result.dataList[0].headImageURL);
      setMiniImageURL(result.dataList[0].miniImageURL);
      setLogoImageURL(result.dataList[0].logoImageURL);

      //return;
    } else {
      // Отправить запрос на создание новой записи

      setSpinner(true);
      const responce = await fetch(
        `${constant.APIСOLLABORATIONIPB}/web/enterprise/`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            idEnterprise: props.uuid,
            idCategory: props.uuid,
            title: currentEnterpriseName,
          }),
        }
      );

      setSpinner(false);

      if (!responce.ok)
        return dispatch({
          type: "SET_DEBUG_MESSAGE",
          payload: responce.statusText,
        });

      const result2 = await responce.json();

      setcollaborateData(result2.data);

      setTitle(result2.data.title);
      setSubTitle(result2.data.subTitle);
      setWebsiteURL(result2.data.websiteURL);
      setFullDescription(result2.data.fullDescription);
      setEnrollBonuses(result2.data.enrollBonuses);
      setAcceptBonuses(result2.data.acceptBonuses);
      setDelivery(result2.data.delivery);
      setHeadImageURL(result2.data.headImageURL);
      setMiniImageURL(result2.data.miniImageURL);
      setLogoImageURL(result2.data.logoImageURL);
    }

    // вытягиваем данные из запроса offer
    console.log(resultCollaboration.dataList.length);
    if (resultCollaboration.dataList.length > 0) {
      // найти среди всех заявок самую свежую

      const newestItem = resultCollaboration.dataList.sort(function (a, b) {
        return new Date(b.lastTimeUpdated) - new Date(a.lastTimeUpdated);
      })[0];
      setOfferData(newestItem);
      console.log("headDescription=", newestItem.headDescription);
      setHeadDescription(newestItem.headDescription);

      //поиск полных данных города по id
      let defCities = [];
      for (let i = 0; i < newestItem.listIDCitiesRes.length; i += 1) {
        defCities.push(
          cities.find((el) => el.idUnique === newestItem.listIDCitiesRes[i])
        );
      }
      let defCitiesModify = defCities.map((el) => {
        return el.name;
      });

      setLoadedCities(defCitiesModify);

      setSelCities(newestItem.listIDCitiesRes.join(","));

      // инициализация при первом старте
    } else {
      const initResponse = await fetch(
        `${constant.APIСOLLABORATIONIPB}/collaboration/offers/web/`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            lastTimeUpdated: moment(),
            idEnterprise: props.uuid,
            listIDCities: "",
            dateStart: "2021-06-16T08:47:42.346Z",
            deadlineDate: "2021-06-16T08:47:42.346Z",
            imageUrl: "init",
            name: "init",
            headDescription: "init",
            fullDescription: "init",
            forOwn: false,
            enrollBonuses: 0,
          }),
        }
      );

      if (!initResponse.ok)
        return dispatch({
          type: "SET_DEBUG_MESSAGE",
          payload: initResponse.statusText,
        });
      const resultInitResponse = await initResponse.json();
      console.log("init", resultInitResponse);

      const responseCollaboration = await fetch(
        `${constant.APIСOLLABORATIONIPB}/collaboration/offers/web/list/${props.uuid}`
      );
      const resultCollaboration = await responseCollaboration.json();
      const newestItem = resultCollaboration.dataList.sort(function (a, b) {
        return new Date(b.lastTimeUpdated) - new Date(a.lastTimeUpdated);
      })[0];
      setOfferData(newestItem);

      console.log("defValue=", newestItem.listIDCitiesRes);
      setLoadedCities(newestItem.listIDCitiesRes);
    }
  };

  // Сохранение новых данных организации - отправка на сервер

  const submitHandler = async () => {
    setSpinner(true);

    const responce = await fetch(
      `${constant.APIСOLLABORATIONIPB}/web/enterprise/`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          idEnterprise: props.uuid,
          idCategory: props.uuid,
          title,
          subTitle,
          fullDescription,
          websiteURL,
          delivery,
          enrollBonuses,
          acceptBonuses,
          headImageURL,
          miniImageURL,
          logoImageURL,
        }),
      }
    );

    const responceCollabCity = await fetch(
      `${constant.APIСOLLABORATIONIPB}/collaboration/offers/web/`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          lastTimeUpdated: moment(),
          idEnterprise: props.uuid,
          listIDCities: selCities,
          dateStart: "2021-06-16T08:47:42.346Z",
          deadlineDate: "2021-06-16T08:47:42.346Z",
          imageUrl: headImageURL,
          name: title,
          headDescription: headDescription,
          fullDescription: fullDescription,
          forOwn: false,
          enrollBonuses: enrollBonuses,
        }),
      }
    );
    setSpinner(false);

    if (!responce.ok || !responceCollabCity.ok) {
      message.error("Ошибка загрузки");
      return dispatch({
        type: "SET_DEBUG_MESSAGE",
        payload: responce.statusText,
      });
    }

    const result = await responce.json();
    const resultresponceCollabCity = await responceCollabCity.json();
    message.info("Данные сохранены");
    console.log("responceCollabCity", responceCollabCity);
  };

  return (
    <Card
      loading={spinner}
      title="Данные организации (Ipbcollaborationcoreapi)"
      bordered={true}
      style={{ width: { winWidth } }}
    >
      <b>uuid: {props.uuid} </b>
      <br />
      <br />
      <Button type="primary" size="large" onClick={getCollaborationData}>
        Получить данные
      </Button>
      <br />
      <>
        <br />
        {offerData && (
          <Card
            title="Просмотр и редактирвание данных"
            bordered={false}
            style={{ width: { winWidth } }}
          >
            <Row>
              <Col span={12}>
                <font size="3"> Название </font>
              </Col>
              <Col span={12}>
                <Input
                  onChange={(e) => setTitle(e.target.value)}
                  type="text"
                  defaultValue={collaborateData.title}
                />
              </Col>
            </Row>

            <br />

            <Row>
              <Col span={12}>
                <font size="3"> Подзаголовок </font>
              </Col>
              <Col span={12}>
                <Input
                  onChange={(e) => setSubTitle(e.target.value)}
                  type="text"
                  defaultValue={collaborateData.subTitle}
                />
              </Col>
            </Row>

            <br />

            <Row>
              <Col span={12}>
                <font size="3"> Полное описание </font>
              </Col>
              <Col span={12}>
                <Input
                  onChange={(e) => setFullDescription(e.target.value)}
                  type="text"
                  defaultValue={collaborateData.fullDescription}
                />
              </Col>
            </Row>
            <br />
            <Row>
              <Col span={12}>
                <font size="3"> HeadDescription </font>
              </Col>
              <Col span={12}>
                <Input
                  onChange={(e) => setHeadDescription(e.target.value)}
                  type="text"
                  defaultValue={offerData.headDescription}
                />
              </Col>
            </Row>

            <br />
            <Row>
              <Col span={12}>
                <font size="3"> Website URL </font>
              </Col>
              <Col span={12}>
                <Input
                  type="url"
                  onChange={(e) => setWebsiteURL(e.target.value)}
                  defaultValue={collaborateData.websiteURL}
                />
              </Col>
            </Row>
            <br />
            <Row>
              <Col span={12}>
                <font size="3"> AcceptBonuses </font>
              </Col>
              <Col span={12}>
                <Input
                  type="number"
                  onChange={(e) => setAcceptBonuses(e.target.value)}
                  defaultValue={collaborateData.acceptBonuses}
                />
              </Col>
            </Row>
            <br />
            <Row>
              <Col span={12}>
                <font size="3"> EnrollBonuses </font>
              </Col>
              <Col span={12}>
                <Input
                  type="number"
                  onChange={(e) => setEnrollBonuses(e.target.value)}
                  defaultValue={collaborateData.enrollBonuses}
                />
              </Col>
            </Row>
            <br />
            <Row>
              <Col span={12}>
                <font size="3"> Delivery </font>
              </Col>
              <Col span={12}>
                <Switch
                  checkedChildren={<CheckOutlined />}
                  unCheckedChildren={<CloseOutlined />}
                  defaultChecked={collaborateData.delivery}
                  onChange={(e) => setDelivery(e)}
                  size="default"
                />
              </Col>
            </Row>
            <br />
            <Row>
              <Col span={12}>
                <font size="3"> Города </font>
              </Col>
              <Col span={12}>
                {loadedCities && (
                  <>
                    <Select
                      mode="multiple"
                      allowClear
                      style={{ width: "100%" }}
                      placeholder="Please select"
                      defaultValue={loadedCities}
                      onChange={(e) => findCityIdbyName(e)}
                    >
                      {cities &&
                        cities.map((el) => {
                          return (
                            <Option
                              key={el.idUnique}
                              value={el.name}
                              label={el.name}
                            >
                              {el.name}
                            </Option>
                          );
                        })}
                    </Select>
                  </>
                )}
              </Col>
            </Row>
            <Divider />
            {/* Модалка для назначения файла */}
            <Modal
              title="Выберите файл"
              style={{ top: 20 }}
              visible={modalFile}
              onOk={() => setModalFile(false)}
              onCancel={() => setModalFile(false)}
            >
              {" "}
              <Uploader uuid={props.uuid} />
            </Modal>
            <Row>
              <Col span={12}>
                <font size="3"> HeadImageURL </font>
                <br />
                <Image
                  fallback="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg=="
                  width={100}
                  src={headImageURL}
                />
              </Col>
              <Col span={12}>
                {headImageURL ? (
                  <a href={headImageURL} target="_blank">
                    {headImageURL}{" "}
                  </a>
                ) : (
                  <b>null</b>
                )}{" "}
                <br />
                <br />
                <Button
                  size="small"
                  onClick={() => {
                    setModalFile(true);
                    dispatch({
                      type: "SET_UPLOAD_URL",
                      payload: "",
                    });
                    dispatch({
                      type: "SET_UPLOAD_TYPE",
                      payload: "HeadImageURL",
                    });
                  }}
                  type="primary"
                >
                  Загрузить
                </Button>
                <span> </span>
                <Button
                  size="small"
                  type="primary"
                  onClick={() => setHeadImageURL(null)}
                >
                  Удалить
                </Button>
              </Col>
            </Row>
            <Divider />
            <Row>
              <Col span={12}>
                <font size="3"> LogoImageURL </font>
                <br />
                <Image
                  fallback="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg=="
                  width={100}
                  src={logoImageURL}
                />
              </Col>
              <Col span={12}>
                {logoImageURL ? (
                  <a href={logoImageURL} target="_blank">
                    {logoImageURL}{" "}
                  </a>
                ) : (
                  <b>null</b>
                )}
                <br />
                <br />
                <Button
                  size="small"
                  onClick={() => {
                    setModalFile(true);
                    dispatch({
                      type: "SET_UPLOAD_URL",
                      payload: "",
                    });
                    dispatch({
                      type: "SET_UPLOAD_TYPE",
                      payload: "LogoImageURL",
                    });
                  }}
                  type="primary"
                >
                  Загрузить
                </Button>
                <span> </span>
                <Button
                  size="small"
                  type="primary"
                  onClick={() => setLogoImageURL(null)}
                >
                  Удалить
                </Button>
              </Col>
            </Row>
            <Divider />
            <Row>
              <Col span={12}>
                <font size="3"> MiniImageURL </font>
                <br />
                <Image
                  fallback="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg=="
                  width={100}
                  src={miniImageURL}
                />
              </Col>
              <Col span={12}>
                {miniImageURL ? (
                  <a href={miniImageURL} target="_blank">
                    {miniImageURL}{" "}
                  </a>
                ) : (
                  <b>null</b>
                )}
                <br />
                <br />
                <Button
                  size="small"
                  onClick={() => {
                    setModalFile(true);
                    dispatch({
                      type: "SET_UPLOAD_URL",
                      payload: "",
                    });
                    dispatch({
                      type: "SET_UPLOAD_TYPE",
                      payload: "MiniImageURL",
                    });
                  }}
                  type="primary"
                >
                  Загрузить
                </Button>
                <span> </span>
                <Button
                  size="small"
                  type="primary"
                  onClick={() => setMiniImageURL(null)}
                >
                  Удалить
                </Button>{" "}
                <span></span>
              </Col>
            </Row>
            <Divider />
            <Button size="large" onClick={submitHandler} type="primary">
              Сохранить
            </Button>
          </Card>
        )}
      </>
    </Card>
  );
};

export default EnterpriseCollaborateInfo;
