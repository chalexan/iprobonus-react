import "./ClientSearch.css";
import { Card } from "antd";
import { Breadcrumb } from "antd";
import { HomeOutlined } from "@ant-design/icons";
import { Input, Button, Form } from "antd";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import data from "../MockUsers/mockUsers";

const ClientSearch = () => {
  const winWidth = window.innerWidth;
  const dispatch = useDispatch();
  const history = useHistory();
  const [inputId, setInputId] = useState("");
  const [inputPhone, setInputPhone] = useState("");
  const [inputFirstName, setInputFirstName] = useState("");
  const [inputLastName, setInputLastName] = useState("");
  const [inputFatherName, setInputFatherName] = useState("");

  const searchIdHandler = () => {
    const filtredArray = data.filter((client) => client.id === inputId);
    dispatch({ type: "ADD_CLIENTS_LIST", payload: filtredArray });
    history.push("/Clients/List");
  };

  const searchPhoneHandler = () => {
    const filtredArray = data.filter((client) => client.phone === inputPhone);
    dispatch({ type: "ADD_CLIENTS_LIST", payload: filtredArray });
    history.push("/Clients/List");
  };

  const searchFioHandler = () => {
    const filtredArray = data.filter(
      (client) =>
        client.firstname === inputFirstName ||
        client.lastname === inputLastName ||
        client.fathername === inputFatherName
    );
    dispatch({ type: "ADD_CLIENTS_LIST", payload: filtredArray });
    history.push("/Clients/List");
  };

  return (
    <div className="site-card-border-less-wrapper">
      <h4>Поиск клиентов</h4>
      <Breadcrumb>
        <Breadcrumb.Item href="">
          <HomeOutlined />
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <span>Клиенты</span>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Поиск клиентов</Breadcrumb.Item>
      </Breadcrumb>
      <br />

      <Card bordered={false} style={{ width: { winWidth } }}>
        <Form layout="horizontal">
          <Form.Item label="По идентификатору  ">
            <div className="flex-container">
              <div className="div-r-30"></div>
              <Input
                size="large"
                placeholder=""
                onChange={(e) => {
                  setInputId(e.target.value);
                }}
              />
              <div className="div-r-50"></div>
              <Button size="large" type="primary" onClick={searchIdHandler}>
                Искать
              </Button>
            </div>
          </Form.Item>

          <Form.Item label="По номеру телефона">
            <div className="flex-container">
              <div className="div-r-50"></div>
              <Input
                size="large"
                placeholder=""
                onChange={(e) => {
                  setInputPhone(e.target.value);
                }}
              />
              <div className="div-r-50"></div>
              <Button size="large" type="primary" onClick={searchPhoneHandler}>
                Искать
              </Button>
            </div>
          </Form.Item>

          <Form.Item label="По ФИО">
            <div className="flex-container">
              <div className="div-r-112"></div>
              <Input
                size="large"
                placeholder="Имя"
                onChange={(e) => {
                  setInputFirstName(e.target.value);
                }}
              />
              <div className="div-r-50"></div>
              <Input
                size="large"
                placeholder="Фамилия"
                onChange={(e) => {
                  console.log(e.target.value);
                  setInputLastName(e.target.value);
                }}
              />
              <div className="div-r-50"></div>
              <Input
                size="large"
                placeholder="Отчество"
                onChange={(e) => {
                  setInputFatherName(e.target.value);
                }}
              />
              <div className="div-r-50"></div>
              <Button size="large" type="primary" onClick={searchFioHandler}>
                Искать
              </Button>
            </div>
          </Form.Item>
        </Form>
      </Card>
    </div>
  );
};

export default ClientSearch;
