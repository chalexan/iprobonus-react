import "./LayoutPage.css";
import { Layout, Menu } from "antd";
import { HomeOutlined } from "@ant-design/icons";
import { useState } from "react";
import { Link } from "react-router-dom";
import MarketingEvent from "../MarketingEvent/MarketingEvent";
import ClientsList from "../ClientsList/ClientsList";
import ClientInfo from "../ClientInfo/ClientInfo";
import ClientSearch from "../ClientSearch/ClientSearch";
import EnterprisesList from "../EnterprisesList/EnterprisesList";
import EnterpriseInfo from "../EnterpriseInfo/EnterpriseInfo";
import EnterpriseCreate from "../EnterpriseCreate/EnterpriseCreate";
import UsersCreate from "../UsersCreate/UsersCreate";
import HeaderInfo from "../HeaderInfo/HeaderInfo";
import ShopsList from "../ShopsList/ShopsList";
import ShopsNew from "../ShopsNew/ShopsNew";
import CityNew from "../CityNew/CityNew";
import ShopInfo from "../ShopInfo/ShopInfo";
import PaymentList from "../PaymentList/PaymentList";
import DebugInfo from "../DebugInfo/DebugInfo";
import ProductInfo from "../ProductInfo/ProductInfo";

const LayoutPage = (props) => {
  const [collapsed, setCollapse] = useState(false);

  const onCollapseHandler = () => {
    setCollapse(!collapsed);
  };
  const { Header, Content, Footer, Sider } = Layout;
  const { SubMenu } = Menu;

  return (
    <div>
      <Layout style={{ minHeight: "100vh" }}>
        <Sider
          //collapsible
          breakpoint="lg"
          collapsedWidth="0"
          width="300px"
          onBreakpoint={(broken) => {
            broken ? setCollapse(true) : setCollapse(false);
          }}
          // collapsed={collapsed}
          // onCollapse={onCollapseHandler}
        >
          <div className="logo" />
          <Menu theme="dark" defaultSelectedKeys={["1"]} mode="inline">
            <SubMenu key="sub3" icon={<HomeOutlined />} title="Организации">
              <Menu.Item key="1">
                <Link to="/Enterprises/List">Список организаций</Link>
              </Menu.Item>
            </SubMenu>
            <SubMenu key="sub4" icon={<HomeOutlined />} title="Документы">
              <Menu.Item key="2">
                <Link to="/PaymentList">Список документов</Link>
              </Menu.Item>
            </SubMenu>
          </Menu>
        </Sider>
        <Layout className="site-layout">
          <Header className="site-layout-background" style={{ padding: 0 }}>
            <HeaderInfo />
          </Header>
          <Content style={{ margin: "0 16px" }}>
            <div className="site-layout-background" style={{ minHeight: 360 }}>
              {/* Рендеринг нужной страницы в заисимости от пропса */}
              {/* Создание нового мероприятия */}
              {props.page === "MarketingEvent" ? <MarketingEvent /> : null}
              {/* Отображение списка клиентов */}
              {props.page === "ClientsList" ? <ClientsList /> : null}
              {/* Отображение личной карточки клиента - помни - там еще в параметре id! */}
              {props.page === "ClientInfo" ? <ClientInfo /> : null}
              {/* Отображение личной карточки клиента - помни - там еще в параметре id! */}
              {props.page === "ClientSearch" ? <ClientSearch /> : null}
              {/* Отображение списка организаций */}
              {props.page === "EnterprisesList" ? <EnterprisesList /> : null}
              {/* Отображение личной карточки клиента - помни - там еще в параметре id! */}
              {props.page === "EnterprisesInfo" ? <EnterpriseInfo /> : null}
              {/* Отображение личной карточки организации - помни - там еще в параметре id! */}
              {props.page === "EnterprisesNew" ? <EnterpriseCreate /> : null}
              {/* Создание нового пользователя*/}
              {props.page === "UsersCreate" ? <UsersCreate /> : null}
              {/* Отображение списка магазинов организации*/}
              {props.page === "ShopsList" ? <ShopsList /> : null}
              {/* Создание нового магазина*/}
              {props.page === "ShopsNew" ? <ShopsNew /> : null}
              {/* Создание нового города*/}
              {props.page === "CityNew" ? <CityNew /> : null}
              {/* Просмотр инфо о магазине*/}
              {props.page === "ShopsInfo" ? <ShopInfo /> : null}
              {/* Просмотр инфо о товаре в магазине*/}
              {props.page === "ProductInfo" ? <ProductInfo /> : null}
              {/* Просмотр документов к оплате*/}
              {props.page === "PaymentList" ? <PaymentList /> : null}
            </div>
          </Content>
          <Footer style={{ textAlign: "center" }}>
            <div>
              <DebugInfo />
            </div>
            <div>ProgressTerra ©2021</div>{" "}
          </Footer>
        </Layout>
      </Layout>
    </div>
  );
};

export default LayoutPage;
