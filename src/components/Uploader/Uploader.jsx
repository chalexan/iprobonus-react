import "./Uploader.css";
import * as constant from "../constant";
import { Card, message } from "antd";
import { Input, Spin, Image, Form } from "antd";
import { FilePdfOutlined } from "@ant-design/icons";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { VscFilePdf } from "react-icons/vsc";
import ReactPlayer from "react-player";

const Uploader = (props) => {
  const winWidth = window.innerWidth;
  const dispatch = useDispatch();
  const [uploadType, setUploadType] = useState("");
  const [spinner, setSpinner] = useState(false);
  const { uploadUrlRedux } = useSelector((state) => state);
  const [uploadUrl, setUploadUrl] = useState(uploadUrlRedux);
  const [tag, setTag] = useState(0);
  const [previewText, setPreviewText] = useState("");

  const uploadHandler = async (e) => {
    if (!e.target.files[0]) return message.error("Файл не выбран");
    dispatch({
      type: "SET_UPLOAD_URL",
      payload: "",
    });
    setUploadUrl("");
    let formdata = new FormData();
    formdata.append("mediafile", e.target.files[0]);
    formdata.append("previewText", previewText);

    const requestOptions = {
      method: "POST",
      body: formdata,
    };
    //Загрузка фото
    if (
      String(e.target.files[0].type) == "image/png" ||
      String(e.target.files[0].type) == "image/jpeg"
    ) {
      setUploadType(0);
      setSpinner(true);
      const responsePicture = await fetch(
        `${constant.APIUPLOADIPB}/web/jpeg/${props.uuid}/image/Dev/${tag}`,
        requestOptions
      );
      setSpinner(false);
      if (!responsePicture.ok) {
        dispatch({
          type: "SET_DEBUG_MESSAGE",
          payload: responsePicture.statusText,
        });
        return message.error("Произошла ошибка");
      }
      if (responsePicture.status !== 200)
        return message.error("Ошибка загрузки");
      const result = await responsePicture.json();
      setUploadUrl(result.data.urlData);
      dispatch({
        type: "SET_UPLOAD_URL",
        payload: result.data.urlData,
      });
    }
    // Загрузка видео
    else if (
      String(e.target.files[0].type) == "video/mp4" ||
      String(e.target.files[0].type) == "video/avi"
    ) {
      setUploadType(1);
      setSpinner(true);
      const responseVideo = await fetch(
        `${constant.APIUPLOADIPB}/web/video/${props.uuid}/video/Dev/${tag}`,
        requestOptions
      );
      setSpinner(false);
      if (!responseVideo.ok) {
        dispatch({
          type: "SET_DEBUG_MESSAGE",
          payload: responseVideo.statusText,
        });
        return message.error("Произошла ошибка");
      }
      if (responseVideo.status !== 200) return message.error("Ошибка загрузки");
      const result2 = await responseVideo.json();
      setUploadUrl(result2.data.urlData);
      dispatch({
        type: "SET_UPLOAD_URL",
        payload: result2.data.urlData,
      });
    }
    // Загрузка документа
    else if (String(e.target.files[0].type) == "application/pdf") {
      setUploadType(2);
      setSpinner(true);
      const responseDoc = await fetch(
        `${constant.APIUPLOADIPB}/web/document/${props.uuid}/pdf/Dev/${tag}`,
        requestOptions
      );
      setSpinner(false);
      if (!responseDoc.ok) {
        dispatch({
          type: "SET_DEBUG_MESSAGE",
          payload: responseDoc.statusText,
        });
        return message.error("Произошла ошибка");
      }
      if (responseDoc.status !== 200) return message.error("Ошибка загрузки");
      const result3 = await responseDoc.json();
      setUploadUrl(result3.data.urlData);
      dispatch({
        type: "SET_UPLOAD_URL",
        payload: result3.data.urlData,
      });
    } else message.error("Тип файла не поддреживается");

    //
  };
  return (
    <Card
      title="Загрузить данные"
      bordered={true}
      style={{ width: { winWidth } }}
    >
      <b>{props.uuid}</b>
      <br />
      <br />
      <Form>
        <Form.Item label="Краткое описание:">
          <Input
            type="text"
            size="small"
            onChange={(e) => setPreviewText(e.target.value)}
          />
        </Form.Item>
        <Form.Item label="Добавьте тег:">
          <Input
            type="number"
            size="small"
            onChange={(e) => setTag(e.target.value)}
          />
        </Form.Item>
        <Form.Item label="Выберите файл:  ">
          <b>Тип фото: jpg, png, видео: mp4, документ: pdf </b>
          <br />
          <br />
          <input type="file" onChange={(e) => uploadHandler(e)} />
        </Form.Item>
        {spinner && <Spin />}
        {uploadUrl ? (
          uploadType == 1 ? (
            <b>
              {" "}
              <ReactPlayer url={uploadUrl} width={150} height={75} />
              <br />
              {uploadUrl}
            </b>
          ) : null
        ) : null}
        {uploadUrl ? (
          uploadType == 0 ? (
            <b>
              {" "}
              <Image
                fallback="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg=="
                alt="image"
                width={100}
                src={uploadUrl}
              />
              <br />
              {uploadUrl}
            </b>
          ) : null
        ) : null}
        {uploadUrl ? (
          uploadType == 2 ? (
            <b>
              {" "}
              <VscFilePdf size="5.5em" />
              <br />
              {uploadUrl}
            </b>
          ) : null
        ) : null}
      </Form>
    </Card>
  );
};

export default Uploader;
