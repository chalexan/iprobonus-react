import "./ClientInfo.css";
import { Card, Row, Col, Button } from "antd";
import { Breadcrumb } from "antd";
import {
  HomeOutlined,
  ContactsTwoTone,
  TagTwoTone,
  CrownTwoTone,
  DashboardTwoTone,
  PhoneTwoTone,
  MailTwoTone,
} from "@ant-design/icons";
import { Link } from "react-router-dom";
import { useEffect, useState } from "react";
import data from "../MockUsers/mockUsers";
import EntityMediaData from "../EntityMediaData/EntityMediaData";

const ClientInfo = (props) => {
  const winWidth = window.innerWidth;
  const [item, setItem] = useState("");
  const [id, setId] = useState("");
  // Достаем id из URL

  useEffect(() => {
    const parts = String(window.location).split("/");
    const id = parts.pop() || parts.pop(); // handle potential trailing slash
    getDataFromId(id);
  }, []);

  // Находим в "базе" элемент с данным id

  const getDataFromId = (id) => {
    // setItem(data.filter((elem) => elem.id === id)[0]);
    setId(id);
  };

  return (
    <div className="site-card-border-less-wrapper">
      <h4>Подробнее о клиенте</h4>
      <Breadcrumb>
        <Breadcrumb.Item href="">
          <HomeOutlined />
        </Breadcrumb.Item>
        {/* <Breadcrumb.Item>
          <Link to="/Clients/List">Клиенты</Link>
        </Breadcrumb.Item> */}
        <Breadcrumb.Item>Подробнее о клиенте</Breadcrumb.Item>
      </Breadcrumb>
      <br />
      <Card
        title="Основная информация"
        bordered={false}
        style={{ width: { winWidth } }}
      >
        <Row>
          <Col span={12}>
            <ContactsTwoTone style={{ fontSize: "20px" }} />
            <font size="3"> ФИО</font>
          </Col>
          <Col span={12}>
            <font size="4" color="#1890FF">
              <b>{item && item.firstname}</b>
              <span> </span>
              <b>{item && item.surname}</b>
              <span> </span>
              <b>{item && item.fathername}</b>
            </font>
          </Col>
        </Row>
        <br />
        <Row>
          <Col span={12}>
            <TagTwoTone style={{ fontSize: "20px" }} />
            <font size="3"> Идентификатор</font>
          </Col>
          <Col span={12}>
            <font size="3" color="#1890FF">
              {/* {item && item.id} */}
              {id}
            </font>
          </Col>
        </Row>
        <br />
        <Row>
          <Col span={12}>
            <CrownTwoTone style={{ fontSize: "20px" }} />
            <font size="3"> Дата рождения</font>
            <br />
            <font size="3" color="#1890FF">
              {item && item.birth}
            </font>
          </Col>
          <Col span={12}>
            <DashboardTwoTone style={{ fontSize: "20px" }} />
            <font size="3"> Дата регистрации</font>
            <br />
            <font size="3" color="#1890FF">
              {item && item.reg}
            </font>
          </Col>
        </Row>
        <br />
        <Row>
          <Col span={12}>
            <PhoneTwoTone style={{ fontSize: "20px" }} />
            <font size="3"> Телефон</font>
            <br />
            <font size="3" color="#1890FF">
              {item && item.phone}
            </font>
          </Col>
          <Col span={12}>
            <MailTwoTone style={{ fontSize: "20px" }} />
            <font size="3"> E-mail</font>
            <br />
            <font size="3" color="#1890FF">
              {item && item.email}
            </font>
          </Col>
        </Row>
        <br />
        <Button type="primary" size="large">
          Редактировать
        </Button>
      </Card>
      <EntityMediaData uuid={id} />
    </div>
  );
};

export default ClientInfo;
