import "./SignUp.css";
import { Card } from "antd";
import { Button, Alert } from "antd";
import Checkbox from "antd/lib/checkbox/Checkbox";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { useRef, useState } from "react";

const SignUp = () => {
  const api = "http://localhost:8080";
  const email = useRef();
  const password = useRef();
  const username = useRef();
  const [errMsg, setErrMsg] = useState("");
  const dispatch = useDispatch();

  const submitHandler = async () => {
    const response = await fetch(`${api}/api/auth/registration`, {
      method: "POST",
      credentials: "include",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        email: email.current.value,
        password: password.current.value,
        username: username.current.value,
      }),
    });
    if (response.status !== 200) {
      const data = await response.json();
      return setErrMsg(data.message);
    }
    const result = await response.json();
    dispatch({
      type: "LOGIN",
      payload: { token: result.token, username: result.username },
    });
  };

  return (
    <div className="center">
      <Card className="center-inside" style={{ width: "360px" }}>
        <div className="center-inside">
          <img src="./assets/img/lock.png" alt="lock" width="50px" />
          <h3>Sign up</h3>
          <input
            placeholder="Username"
            type="text"
            class="ant-input ant-input-lg"
            ref={username}
            size="40"
          />
          <br />
          <input
            placeholder="E-Mail"
            type="email"
            class="ant-input ant-input-lg"
            ref={email}
          />
          <br />
          <input
            placeholder="Password"
            type="password"
            class="ant-input ant-input-lg"
            ref={password}
          />
          <br />
          {errMsg ? (
            <div>
              <Alert message={errMsg} type="error" /> <br />
            </div>
          ) : null}
        </div>

        <p>
          <Checkbox>Send me the Newsletter weekly.</Checkbox>
        </p>
        <div className="center-inside">
          <Button type="primary" size="large" onClick={submitHandler}>
            Sign up
          </Button>
          <br />
          <div>
            Already have an account? <Link to="/login">Login</Link>
          </div>
        </div>
      </Card>
    </div>
  );
};

export default SignUp;
