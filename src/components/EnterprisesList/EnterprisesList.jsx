import "./EnterprisesList.css";
import * as constant from "../constant";
import { Card } from "antd";
import { Breadcrumb } from "antd";
import { HomeOutlined, SearchOutlined } from "@ant-design/icons";
import { Input, Button, Space, Table } from "antd";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, Link } from "react-router-dom";
import Highlighter from "react-highlight-words";

const EnterprisesList = () => {
  const winWidth = window.innerWidth;
  const { Search } = Input;
  const history = useHistory();
  const { enterprises } = useSelector((state) => state);
  const { changes } = useSelector((state) => state);
  const [dataSource, setDataSource] = useState(enterprises);
  const [spinner, setSpinner] = useState(false);
  const dispatch = useDispatch();
  useEffect(() => {
    getEnterprises();
    getRoles();
    setDataSource(enterprises);
  }, [changes]);

  // запрос на получение списка ролей

  const getRoles = async () => {
    const token = localStorage.getItem("token");
    const api = constant.API;
    const response = await fetch(`${api}/api/enterprise/role`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `bearer ${token}`,
        AccessKey: constant.ACCESSKEY,
      },
    });

    if (response.status === 401 || response.status === 403) {
      return dispatch({ type: "LOGOUT" });
    }
    if (!response.ok)
      return dispatch({
        type: "SET_DEBUG_MESSAGE",
        payload: response.statusText,
      });
    const result = await response.json();
    if (result.result.status !== 0)
      return dispatch({
        type: "SET_DEBUG_MESSAGE",
        payload: result.result.message,
      });
    dispatch({ type: "ADD_ROLES", payload: result.dataList });
    dispatch({ type: "SET_DEBUG_MESSAGE", payload: response.statusText });
  };

  // запрос на получение списка организаций

  const getEnterprises = async () => {
    const token = localStorage.getItem("token");
    const api = constant.API;
    setSpinner(true);
    const response = await fetch(`${api}/api/enterprise/`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `bearer ${token}`,
        AccessKey: constant.ACCESSKEY,
      },
    });
    setSpinner(false);
    if (response.status === 401 || response.status === 403) {
      return dispatch({ type: "LOGOUT" });
    }
    if (!response.ok)
      return dispatch({
        type: "SET_DEBUG_MESSAGE",
        payload: response.statusText,
      });
    const result = await response.json();
    if (result.result.status !== 0)
      return dispatch({
        type: "SET_DEBUG_MESSAGE",
        payload: result.result.message,
      });
    dispatch({ type: "ADD_ENTERPRISES_LIST", payload: result.dataList });
    dispatch({ type: "SET_DEBUG_MESSAGE", payload: response.statusText });
    setDataSource(enterprises);
  };

  //Поиск по всем колонкам

  const filterAllData = (e) => {
    const filteredData = enterprises.filter(
      (entry) => entry.idUnique.includes(e) || entry.name.includes(e)
    );
    setDataSource(filteredData); // пофиксить!
  };

  // Поиск по колонке

  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  let searchInput;

  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node) => {
            searchInput = node;
          }}
          placeholder={`Введите запрос`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Поиск
          </Button>
          <Button
            onClick={() => handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Сброс
          </Button>
          <Button
            type="link"
            size="small"
            onClick={() => {
              confirm({ closeDropdown: false });
              setSearchText(selectedKeys[0]);
              setSearchedColumn(dataIndex);
            }}
          ></Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex]
            .toString()
            .toLowerCase()
            .includes(value.toLowerCase())
        : "",
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.select(), 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      ),
  });

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();

    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText("");
  };

  // Конец поиска по колонке

  // Обработчик перехода на подробности

  const handleDetails = (id) => {
    dispatch({ type: "UPDATE" });
    history.push(`/Enterprises/Info/${id}`);
  };

  // Описание столбцов

  const columns = [
    {
      title: "ID",
      dataIndex: "idUnique",
      ...getColumnSearchProps("idUnique"),
      sorter: {
        compare: (a, b) => a.key - b.key,
      },
      responsive: ["lg"],
    },
    {
      title: "Название",
      dataIndex: "name",
      ...getColumnSearchProps("name"),
      sorter: {
        compare: (a, b) => a.key - b.key,
      },
    },
    {
      title: "Действие",
      dataIndex: "",
      key: "x",
      render: (_, record) => (
        <a onClick={() => handleDetails(record.idUnique)}>Подробнее</a>
      ),
    },
  ];

  return (
    <div className="site-card-border-less-wrapper">
      <h4>Список организаций</h4>
      <Breadcrumb>
        <Breadcrumb.Item href="">
          <HomeOutlined />
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <span>Организации</span>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Список организаций</Breadcrumb.Item>
      </Breadcrumb>
      <br />
      <Card
        loading={spinner}
        title="Организации"
        bordered={false}
        style={{ width: { winWidth } }}
      >
        <div className="left-right">
          <div>
            <Button type="primary" size="large">
              <Link to="/Enterprises/New">Создать организацию</Link>
            </Button>{" "}
          </div>
          <div>
            {/* <Search
              placeholder="Поиск..."
              size="large"
              onSearch={filterAllData}
              enterButton
            /> */}
          </div>
        </div>
        <br />
        <Table
          columns={columns}
          // Пофиксить dataSource - все же попробывть вытянуть из useState
          dataSource={enterprises}
          pagination={{ position: ["none", "bottomCenter"] }}
        />
      </Card>
    </div>
  );
};
export default EnterprisesList;
