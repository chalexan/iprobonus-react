const faker = require("faker");
const data = [];
for (let i = 0; i < 55; i += 1) {
    data.push({
        key: i,
        id: faker.datatype.uuid(),
        firstname: faker.name.firstName(),
        surname: faker.name.middleName(),
        fathername: faker.name.lastName(),
        phone: faker.phone.phoneNumber(),
        email: faker.internet.email(),
        birth: String(faker.date.past()),
        reg: String(faker.date.past()),
    });
}
console.log('ALL DATA ->>>> ', data);
module.exports = data;