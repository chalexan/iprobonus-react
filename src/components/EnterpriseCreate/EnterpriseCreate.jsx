import "./EnterpriseCreate.css";
import * as constant from "../constant";
import { Card, Form, Input, Button, Alert } from "antd";
import { Breadcrumb, message } from "antd";
import { HomeOutlined } from "@ant-design/icons";
import { useHistory } from "react-router-dom";
import { useState } from "react";
import { useDispatch } from "react-redux";

const EnterpriseCreate = () => {
  const winWidth = window.innerWidth;
  const [name, setInputName] = useState("");
  const [errMsg, setErrMessage] = useState("");
  const dispatch = useDispatch();
  const history = useHistory();
  //создание новой организации

  const createEnterpriseHandler = async () => {
    const token = localStorage.getItem("token");
    const api = constant.API;
    const accessKey = constant.ACCESSKEY;
    if (name !== "") {
      const response = await fetch(`${api}/api/enterprise/${name}`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `bearer ${token}`,
          AccessKey: constant.ACCESSKEY,
        },
      });
      if (response.status === 401 || response.status === 403) {
        return dispatch({ type: "LOGOUT" });
      }
      const result = await response.json();
      // проверка на корректность ответа
      if (result.result.status === 0) {
        message.success("Организация успешно создана");
        dispatch({ type: "UPDATE" });
        history.push("/Enterprises/List");
      } else message.error(result.result.message);
    } else setErrMessage("Заполните название организации");
  };

  return (
    <div className="site-card-border-less-wrapper">
      <h4>Cоздать организацию</h4>
      <Breadcrumb>
        <Breadcrumb.Item href="">
          <HomeOutlined />
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <span>Организации</span>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Создать организацию</Breadcrumb.Item>
      </Breadcrumb>
      <br />

      <Card bordered={false} style={{ width: { winWidth } }}>
        <Form layout="horizontal">
          <Form.Item label="Название организации ">
            <div className="flex-container">
              <div className="div-r-30"></div>
              <Input
                size="large"
                placeholder=""
                onChange={(e) => {
                  setInputName(e.target.value);
                }}
              />
              <div className="div-r-50"></div>
              <Button
                size="large"
                type="primary"
                onClick={createEnterpriseHandler}
              >
                Создать
              </Button>
            </div>
          </Form.Item>
          {errMsg ? <Alert message={errMsg} type="error" /> : null}
        </Form>
      </Card>
    </div>
  );
};

export default EnterpriseCreate;
