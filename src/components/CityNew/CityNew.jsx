import cities from "../cities";
import "./CityNew.css";
import * as constant from "../constant";
import { Card, Form, Button, Modal, Spin, Alert, message } from "antd";
import { Breadcrumb, Input, Image, Select } from "antd";
import { HomeOutlined } from "@ant-design/icons";
import { Link, useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";

import { useState, useEffect } from "react";

const CityNew = () => {
  const winWidth = window.innerWidth;
  const { Option } = Select;
  const { currentEnterpriseName } = useSelector((state) => state);
  const dispatch = useDispatch();
  const history = useHistory();
  const [city, setCity] = useState(null);

  // Нахождение широты и долготы центра выбранного города

  const setCityHandler = (e) => {
    const filter = cities.cities.filter((el) => el.Город == e);
    setCity(filter);
  };

  // Добавление нового города
  const createCityHandler = async () => {
    if (!city) return message.error("Город не выбран");

    const token = localStorage.getItem("token");
    const api = constant.APISHOP;

    const response = await fetch(`${api}/iecommercecore/api/v1/shop/city`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `bearer ${token}`,
        AccessKey: constant.ACCESSKEY,
      },
      body: JSON.stringify({
        name: city[0].Город,
        latitudeCenter: city[0].Широта,
        longitudeCenter: city[0].Долгота,
        radius: 0,
      }),
    });
    if (response.status === 401 || response.status === 403) {
      return dispatch({ type: "LOGOUT" });
    }
    if (!response.ok) {
      dispatch({
        type: "SET_DEBUG_MESSAGE",
        payload: response.statusText,
      });
      return message.error("Произошла ошибка");
    }
    const result = await response.json();
    // проверка на корректность ответа
    if (result.status === 0) {
      message.success("Город добавлен");
      dispatch({
        type: "SET_DEBUG_MESSAGE",
        payload: response.statusText,
      });
      setTimeout(() => history.goBack(), 1000);
    } else message.error(result.message);
  };

  return (
    <div className="site-card-border-less-wrapper">
      <h4>Добавление города</h4>
      <Breadcrumb>
        <Breadcrumb.Item href="">
          <HomeOutlined />
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <Link to="/Enterprises/List">Организации</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <span>{currentEnterpriseName}</span>
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <Link to="/Shops/List">Магазины</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Добавление города</Breadcrumb.Item>
      </Breadcrumb>
      <br />
      <Card bordered={true} style={{ width: { winWidth } }}>
        <Form layout="vertical">
          <Form.Item label="Выберите город из списка">
            <Select showSearch onChange={setCityHandler}>
              {cities &&
                cities.cities.map((el) => {
                  return (
                    <Option key={Math.random()} value={el.Город}>
                      {el.Город}
                    </Option>
                  );
                })}
            </Select>
          </Form.Item>
          {city && (
            <>
              {" "}
              Широта: {city[0].Широта} <br />
            </>
          )}

          {city && (
            <>
              {" "}
              Долгота: {city[0].Долгота} <br />
            </>
          )}
        </Form>
        <br />
        <Button type="primary" size="large" onClick={createCityHandler}>
          Добавить город
        </Button>
      </Card>
    </div>
  );
};

export default CityNew;
