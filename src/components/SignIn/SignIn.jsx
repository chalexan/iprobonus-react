import "./SignIn.css";
import { Card } from "antd";
import { Button, Alert } from "antd";
import Checkbox from "antd/lib/checkbox/Checkbox";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { useRef, useState } from "react";
require("dotenv").config();
const SignIn = () => {
  const api = "http://84.201.184.154:5085";
  const email = useRef();
  const password = useRef();
  const dispatch = useDispatch();
  const [errMsg, setErrMsg] = useState("");

  const submitHandler = async () => {
    const response = await fetch(
      `${api}/token?username=${email.current.value}&password=${password.current.value}`,
      {
        method: "POST",
        headers: { "Content-Type": "application/json" },
      }
    );
    if (response.status !== 200) {
      const data = await response.json();
      return setErrMsg(data.errorText);
    }
    const result = await response.json();
    localStorage.setItem("token", result.access_token);
    localStorage.setItem("username", result.username);

    dispatch({
      type: "LOGIN",
      payload: { token: result.access_token, username: result.username },
    });
  };

  return (
    <div className="center">
      <Card className="center-inside" style={{ width: "360px" }}>
        <div className="center-inside">
          <img src="./assets/img/lock.png" alt="lock" width="50px" />
          <h3>Login</h3>
          <input
            placeholder="E-Mail"
            type="email"
            class="ant-input ant-input-lg"
            ref={email}
            size="40"
          />
          <br />
          <input
            placeholder="Password"
            type="password"
            class="ant-input ant-input-lg"
            ref={password}
          />
          <br />
          {errMsg ? (
            <div>
              <Alert message={errMsg} type="error" /> <br />
            </div>
          ) : null}
        </div>
        <p>
          <Checkbox>Save credentials</Checkbox>
        </p>
        <div className="center-inside">
          <Button type="primary" size="large" onClick={submitHandler}>
            LOGIN
          </Button>
          <br />
          <div>
            Forgot password? <a href="#">Reset</a>
          </div>
          <div>
            Don’t have an account? <Link to="/registration">Signup</Link>
          </div>
        </div>
      </Card>
    </div>
  );
};

export default SignIn;
