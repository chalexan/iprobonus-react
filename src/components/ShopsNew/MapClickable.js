import { Map, TileLayer, Marker, Popup } from 'react-leaflet';
import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

const MapClickable = () => {
  const [position, setPosition] = useState([55.7522200, 37.6155600]);
  const dispatch = useDispatch();

  const handleClick = async (e) => {
    setPosition(e.latlng);
    dispatch({ type: "SET_CURRENT_COORDS", payload: e.latlng })
  }
  return (
    <Map center={position} zoom={14} scrollWheelZoom={false} onClick={handleClick}>
      <TileLayer
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <Marker position={position}>
      </Marker>
    </Map>
  );
}

export default MapClickable;
