import "./ShopsNew.css";
import * as constant from "../constant";
import moment from "moment";
import { Card, Form, Button, Modal, Spin, Alert, message } from "antd";
import { Breadcrumb, Input, Image, Select, Checkbox } from "antd";
import { HomeOutlined } from "@ant-design/icons";
import { Link, useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import TimePicker from "rc-time-picker";
import FileToBase64 from "dd-file-to-base64";
import { useState, useEffect } from "react";
import MapClickable from "./MapClickable";
import Uploader from "../Uploader/Uploader";

const ShopsNew = () => {
  const winWidth = window.innerWidth;
  const { Option } = Select;
  const history = useHistory();
  const dispatch = useDispatch();
  const { currentEnterpriseId } = useSelector((state) => state);
  const { currentEnterpriseName } = useSelector((state) => state);
  const { currentCoords } = useSelector((state) => state);
  const [modalVis, setModalVis] = useState(false);
  const [uploadErr, setUploadErr] = useState("");
  const [uploadUrl, setUploadUrl] = useState("no-url");
  const [spinner, setSpinner] = useState("");
  const [name, setName] = useState("");
  const [adress, setAdress] = useState("");
  const [contacts, setContacts] = useState("");
  const [timeOpen, setTimeOpen] = useState(moment());
  const [timeClose, setTimeClose] = useState(moment());
  const [cities, setCities] = useState([]);
  const [city, setCity] = useState("");
  // Для опциальных параметров
  const [addCoords, setAddCoords] = useState(false);
  const [addPic, setAddPic] = useState(false);
  // Для ошибок ввода данных
  const [errName, setErrName] = useState(null);
  const [errAdress, setErrAdress] = useState(null);
  const [errContacts, setErrContacts] = useState(null);
  const [errTime, setErrTime] = useState(null);
  const [errCoords, setErrCoords] = useState(null);
  const [errPic, setErrPic] = useState(null);

  useEffect(() => {
    getCities();
  }, []);

  // Перенаправление на добавление нового города

  const createCityHandler = () => {
    // history.push('');
  };

  // Запрос на получение списка городов

  const getCities = async () => {
    const token = localStorage.getItem("token");
    const api = constant.APISHOP;

    const response = await fetch(
      `${api}/iecommercecore/api/v1/shop/city/list`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `bearer ${token}`,
          AccessKey: constant.ACCESSKEY,
        },
      }
    );
    if (response.status === 401) {
      return dispatch({ type: "LOGOUT" });
    }
    if (response.status !== 200) return;
    const result = await response.json();
    console.log("cities", result.dataList);
    setCities(result.dataList);
  };

  // Сохранение картинки на сервере

  const uploadHandler = async (e) => {
    setUploadUrl("");
    setErrPic("");

    console.log("fileName type", e.target.files[0].type);

    // if (
    //  String(e.target.files[0].type) !== "image/png" &&
    //   String(e.target.files[0].type) !== "image/jpeg"
    // )
    //   return setErrPic("Неподдерживаемый тип файла");

    //application/pdf
    //video/mp4

    // if (e.target.files[0].size / 1024 / 1024 > 4)
    //   return setErrPic("Размер файла более 4 Мб");

    let formdata = new FormData();
    formdata.append("mediafile", e.target.files[0]);

    const requestOptions = {
      method: "POST",
      body: formdata,
    };

    setSpinner(true);

    // const response = await fetch(
    //   `${constant.APIUPLOADIPB}/web/jpeg/${currentEnterpriseId}/image/Dev`,
    //   requestOptions
    // );

    setSpinner(false);
    // if (!response.ok) {
    //   dispatch({
    //     type: "SET_DEBUG_MESSAGE",
    //     payload: response.statusText,
    //   });
    //   return message.error("Произошла ошибка");
    // }
    // if (response.status !== 200) return setUploadErr("Ошибка загрузки");
    // const result = await response.json();
    // setUploadUrl(result.data.urlData);
  };

  // Создание магазина

  const createShopHandler = async () => {
    // Проверка формы

    if (!name) return setErrName("Введите название магазина");
    setErrName("");
    if (!adress) return setErrAdress("Введите адрес магазина");
    setErrAdress("");
    if (!city) return setErrAdress("Выберите город");
    setErrAdress("");
    if (!contacts) return setErrContacts("Введите контаты");
    setErrContacts("");
    if (!timeOpen || !timeClose) return setErrTime("Введите время");
    setErrTime("");
    if (!currentCoords.lng) return setErrCoords("Укажите координаты");
    setErrCoords("");
    if (!uploadUrl) return setErrPic("Загрузите фото");
    setErrPic("");

    // Запрос на сервер

    const token = localStorage.getItem("token");
    const api = constant.APISHOP;

    const response = await fetch(`${api}/iecommercecore/api/v1/shop`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `bearer ${token}`,
        AccessKey: constant.ACCESSKEY,
      },
      body: JSON.stringify({
        idEnterprise: currentEnterpriseId,
        name: name,
        address: adress,
        dateOpen: timeOpen,
        dateClose: timeClose,
        idCtiy: city,
        contacts: contacts,
        latitude: currentCoords.lat,
        longitude: currentCoords.lng,
        imageURL: uploadUrl,
      }),
    });
    if (response.status === 401) {
      return dispatch({ type: "LOGOUT" });
    }
    if (!response.ok) {
      dispatch({
        type: "SET_DEBUG_MESSAGE",
        payload: response.statusText,
      });
      return message.error("Произошла ошибка");
    }
    const result = await response.json();
    // проверка на корректность ответа
    if (result.status === 0) {
      message.success("Магазин создан");
      dispatch({
        type: "SET_DEBUG_MESSAGE",
        payload: response.statusText,
      });
    } else message.error(result.message);
  };

  return (
    <div className="site-card-border-less-wrapper">
      <h4>Создание магазина</h4>
      <Breadcrumb>
        <Breadcrumb.Item href="">
          <HomeOutlined />
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <Link to="/Enterprises/List">Организации</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <span>{currentEnterpriseName}</span>
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <Link to="/Shops/List">Магазины</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Создание магазина</Breadcrumb.Item>
      </Breadcrumb>
      <br />
      <Card bordered={true} style={{ width: { winWidth } }}>
        <Form layout="vertical">
          <Form.Item label="Организация:  ">
            <div className="flex-container">
              <b>
                {currentEnterpriseName}({currentEnterpriseId})
              </b>
            </div>
          </Form.Item>
          <Form.Item label="Название магазина:  ">
            <Input
              type="text"
              onChange={(e) => setName(e.target.value)}
            ></Input>
            {errName ? (
              <>
                <br /> <br /> <Alert type="error" message={errName} />
              </>
            ) : null}
          </Form.Item>
          <Form.Item label="Адрес магазина:  ">
            <div className="left-right">
              <div>
                Выберите город: <span> </span>
                <Select style={{ width: 200 }} onChange={(e) => setCity(e)}>
                  {cities &&
                    cities.map((el) => {
                      return (
                        <Option key={Math.random()} value={el.idUnique}>
                          {el.name}
                        </Option>
                      );
                    })}
                </Select>
              </div>
              <div>
                <Button
                  type="primary"
                  onClick={() => history.push("/Shops/City/New")}
                >
                  Добавить город
                </Button>
              </div>
            </div>
            <br />
            Адрес:
            <Input
              type="text"
              onChange={(e) => setAdress(e.target.value)}
            ></Input>
            {errAdress ? (
              <>
                <br /> <br /> <Alert type="error" message={errAdress} />
              </>
            ) : null}
          </Form.Item>
          <Form.Item label="Контакты:  ">
            <Input
              type="text"
              onChange={(e) => setContacts(e.target.value)}
            ></Input>
            {errContacts ? (
              <>
                <br /> <br /> <Alert type="error" message={errContacts} />
              </>
            ) : null}
          </Form.Item>
          <Checkbox onChange={(e) => setAddCoords(e.target.checked)}>
            {" "}
            Добавить координаты{" "}
          </Checkbox>
          <br /> <br />
          {addCoords && (
            <Card bordered={true} style={{ width: { winWidth } }}>
              <Form.Item label="Координаты магазина:  ">
                <Button type="default" onClick={() => setModalVis(true)}>
                  Указать координаты
                </Button>
                {currentCoords.lat && (
                  <>
                    <br />
                    <br />
                    Широта: <b>{currentCoords.lat}</b>
                    <br />
                    Долгота: <b>{currentCoords.lng}</b>
                  </>
                )}
                {errCoords ? (
                  <>
                    <br /> <br />
                    <Alert type="error" message={errCoords} />
                  </>
                ) : null}
              </Form.Item>
            </Card>
          )}
          {/* Модалка для карты */}
          <Modal
            title="Укажите точку с координатами"
            style={{ top: 20 }}
            visible={modalVis}
            onOk={() => setModalVis(false)}
            onCancel={() => setModalVis(false)}
          >
            <MapClickable />
          </Modal>
          <Checkbox onChange={(e) => setAddPic(e.target.checked)}>
            {" "}
            Добавить медиаданные{" "}
          </Checkbox>
          {addPic && <Uploader uuid={currentEnterpriseId} />}
          <br /> <br />
        </Form>
        <br />
        <Button type="primary" onClick={createShopHandler}>
          Создать магазин
        </Button>
      </Card>
    </div>
  );
};

export default ShopsNew;
