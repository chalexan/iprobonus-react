import { useSelector } from "react-redux";
import { Alert } from "antd";

const DebugInfo = () => {
  const { debugMessage } = useSelector((state) => state);

  return <Alert type="info" message={debugMessage} />;
};

export default DebugInfo;
