import SignIn from "../../components/SignIn/SignIn";
import "./style.css";

const SignUpPage = () => {
  return (
    <>
      <div class="auth-wrapper">
        <div class="auth-content">
          <div class="auth-bg">
            <span class="r"></span>
            <span class="r s"></span>
            <span class="r s"></span>
            <span class="r"></span>
          </div>
          <SignIn />
        </div>
      </div>
    </>
  );
};

export default SignUpPage;
