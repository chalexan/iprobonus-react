import SignUp from "../../components/SignUp/SignUp";
import "./style.css";

const SignInPage = () => {
  return (
    <>
      <div class="auth-wrapper">
        <div class="auth-content">
          <div class="auth-bg">
            <span class="r"></span>
            <span class="r s"></span>
            <span class="r s"></span>
            <span class="r"></span>
          </div>
          <SignUp />
        </div>
      </div>
    </>
  );
};

export default SignInPage;
