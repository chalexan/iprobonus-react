import * as action from './actionTypes';

const initState = {
    isLogin: false,
    username: '',
    token: '',
    data: [],
    enterprises: [],
    users: [],
    changes: '',
    currentEnterpriseId: '',
    currentEnterpriseName: '',
    currentCoords: { lat: 0.001, lng: 0.001 },
    debugMessage: "OK",
    uploadUrlRedux: "",
    uploadType: "",
    currentShopId: "",
    currentNomenklatura: "",
};

export default function reducer(state = initState, { type, payload }) {
    switch (type) {
        case action.LOGIN:
            return { ...state, isLogin: true, username: payload.username, token: payload.token };
        case action.ADD_CLIENTS_LIST:
            return { ...state, data: payload }
        case action.ADD_ENTERPRISES_LIST:
            return { ...state, enterprises: payload }
        case action.ADD_USERS_LIST:
            return { ...state, enterprises: payload }
        case action.LOGOUT:
            localStorage.removeItem("access_token");
            return { ...state, isLogin: false, username: "", token: "" }
        case action.UPDATE:
            return { ...state, changes: Math.random() }
        case action.SET_CURRENT_ENTERPRISE:
            return { ...state, currentEnterpriseId: payload.enterpriseId, currentEnterpriseName: payload.enterpriseName }
        case action.SET_CURRENT_SHOP_ID:
            return { ...state, currentShopId: payload }
        case action.SET_CURRENT_NOMENKLATURA:
            return { ...state, currentNomenklatura: payload }
        case action.ADD_ROLES:
            return { ...state, roles: payload }
        case action.SET_CURRENT_COORDS:
            return { ...state, currentCoords: payload }
        case action.SET_DEBUG_MESSAGE:
            return { ...state, debugMessage: payload }
        case action.SET_UPLOAD_URL:
            return { ...state, uploadUrlRedux: payload }
        case action.SET_UPLOAD_TYPE:
            return { ...state, uploadType: payload }
        default:
            return state;
    }
}