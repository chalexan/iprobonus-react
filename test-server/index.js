const express = require('express');
const cors = require('cors');
const User = require('./models/user');
const Payment = require('./models/payment');
const sha256 = require('sha256');
const jwt = require('jsonwebtoken');
const cookieParser = require('cookie-parser');

const { connect, dbPath } = require('./connectDb');

const app = express();
connect();
const secret = 'SECRET WORD';

app.use(
    cors({
        origin: true,
        credentials: true,
    })
);
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

const generateAccessToken = (id) => {
    const payload = { id };
    return jwt.sign(payload, secret, { expiresIn: '24h' });
}
Payment.collection.drop();
for (let i = 0; i < 10; i++) {
    Payment.create({
        idUnique: String(Math.random()),
        idClientAmbassador: String(Math.random()),
        dateСonfirmation: null,
        idOfficerConfirmation: String(Math.random()),
        sum: 400
    });
}

app.post('/api/auth/registration', async (req, res) => {
    const { username, email, password } = req.body;
    console.log(req.body);
    const candidate = await User.findOne({ username });
    if (candidate) {
        return res.status(400).json({ message: 'Такой пользователь уже существует' });
    }
    const passwordHashed = sha256(password);
    const user = await User.create({ username, password: passwordHashed, email });
    const token = generateAccessToken(user._id);
    res.cookie('token', token);
    res.status(200).json({ token, username: user.username });
})

app.post('/api/auth/login', async (req, res) => {
    const { email, password } = req.body;
    const user = await User.findOne({ email });
    if (!user) {
        return res.status(401).json({ message: 'Пользователь не найден' });
    }
    if (sha256(password) !== user.password) {
        return res.status(401).json({ message: 'Пароль неверный' });
    }
    const token = generateAccessToken(user._id);
    res.cookie('token', token);
    res.status(200).json({ token, username: user.username });
})

app.get('/api/auth/check/:token', async (req, res) => {
    const cookieToken = req.params.token;
    if (!cookieToken) {
        return res.status(400).json({ message: 'Нет куки' });
    }
    const decoded = jwt.verify(cookieToken, secret);
    const user = await User.findById(decoded.id);
    if (!user) {
        return res.status(400).json({ message: 'Неверный токен - нет соответствующего пользователя' });
    }
    res.status(200).json({ message: 'OK', username: user.username });
})

app.get('/moneyout/list', async (req, res) => {

    const list = await Payment.find();
    res.status(200).json({ dataList: list });
})

app.post(`/moneyout/confirm/:idUnique/:idOfficerConfirmation`, async (req, res) => {

    console.log(req.params);
    await Payment.updateOne({ idUnique: req.params.idUnique },
        { $set: { dateСonfirmation: '3' } })
    const list = await Payment.find();
    res.status(200).json({ dataList: list });
})


app.listen(8080, console.log('Server Started at port 8080'));