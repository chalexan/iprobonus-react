const mongoose = require('mongoose');

const paymentSchema = new mongoose.Schema({
    idUnique: String,
    idClientAmbassador: String,
    dateСonfirmation: String,
    idOfficerConfirmation: String,
    sum: Number,
});

module.exports = mongoose.model('Payment', paymentSchema);